import type { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'

// Model
import BidangKonten from 'App/Models/BidangKonten';

export default class BidangKontensController {
  public async index({response}: HttpContextContract) {
    try {
      const bidangKonten = await BidangKonten.all();

      if (bidangKonten.length == 0){
        response.ok({message: 'Zero'})
        return
      }

      response.ok({message: "Index Ok", bidangKonten: bidangKonten })
    } catch (error) {
      response.badRequest({message: error});
      console.log({message: error});
      
    }
  }

  public async store({request, response}: HttpContextContract) {
    try {

      const {nama} = request.body();
      const bidangKonten = new BidangKonten();

      bidangKonten.nama = nama;

      await bidangKonten.save();

      console.log({nama});
      

      response.ok({message: "Store Ok"})
    } catch (error) {
      response.badRequest({message: error});
      console.log({message: error});
      
    }
  }

  public async show({response, params}: HttpContextContract) {
  try {
    const id = params.id;
    const bidangKonten = await BidangKonten.find(id);

    if (!bidangKonten){
      response.badRequest({message: 'Bidang Konten Tidak Tersedia'});
      return;
    }
   
    response.ok({message: "Show Ok", bidangKonten: bidangKonten})
  } catch (error) {
    response.badRequest({message: error});
    console.log({message: error});
    
  }}  

  public async update({request, response, params}: HttpContextContract) {
    try {

      const id = params.id;
      const bidangKonten = await BidangKonten.find(id);

      const {nama} = request.body();

      if (!bidangKonten){
        response.badRequest({message: 'Bidang Konten Tidak Tersedia'});
        return;
      }

      if (nama){
        bidangKonten.nama = nama;
        bidangKonten.save();
      }

      response.ok({message: "Update Ok", bidangKonten: bidangKonten})
    } catch (error) {
      response.badRequest({message: error});
      console.log({message: error});
      
    }
  }

  public async destroy({response, params}: HttpContextContract) {
    try {
      const id = params.id;
      const bidangKonten = await BidangKonten.find(id);

      if (!bidangKonten){
        response.badRequest({message: 'Bidang Konten Tidak Tersedia'});
        return;
      }

      bidangKonten.delete();
      response.ok({message: "Destroy Ok"})

    } catch (error) {
      response.badRequest({message: error});
      console.log({message: error});
      
    }
  }
}
