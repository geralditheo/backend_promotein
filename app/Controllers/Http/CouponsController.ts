import type { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'

import Coupon from 'App/Models/Coupon'
import { schema } from '@ioc:Adonis/Core/Validator'

export default class CouponsController {
  public async index({response}: HttpContextContract) {
    try {
      const coupons = await Coupon.all();

      if (coupons.length == 0){
        return response.ok({message: 'Coupon Zero'});
      }

      return response.ok({coupons: coupons});

    } catch (error) {
      return response.badRequest(error);
    }
  }

  // public async create({}: HttpContextContract) {}

  public async store({request, response}: HttpContextContract) {
    const coupon_validator = schema.create({
      coupon_name: schema.string(),
      coupon_limit_number: schema.number.optional(),
      coupon_limit_date: schema.date.optional(),
      discount: schema.number.optional(),
    })

    try {
      const payload = await request.validate({schema: coupon_validator});
      const coupon = new Coupon();

      if (payload.coupon_name){
        coupon.coupon_name = payload.coupon_name;
      }

      if (payload.coupon_limit_number){
        coupon.coupon_limit_number = payload.coupon_limit_number;
        coupon.counter = 0;
      }

      if (payload.coupon_limit_date){
        coupon.coupon_limit_date = payload.coupon_limit_date;
      }

      coupon.discount = 0;
      if (payload.discount){
        coupon.discount = payload.discount;
      }

      await coupon.save();
      return response.ok({coupon: coupon});
    } catch (error) {
      return response.badRequest(error);
    }
  }

  public async show({response, params}: HttpContextContract) {
    try {
      const { coupon_id } = params;
      const coupon = await Coupon.find(coupon_id);

      if (!coupon){
        return response.badRequest({message: 'No Coupon'});
      }

      return response.ok({coupon: coupon});
    } catch (error) {
      return response.badRequest(error);
    }
  }

  // public async edit({}: HttpContextContract) {}

  public async update({request, response, params}: HttpContextContract) {

    const coupon_validator = schema.create({
      coupon_name: schema.string.optional(),
      coupon_limit_number: schema.number.optional(),
      coupon_limit_date: schema.date.optional(),
      counter: schema.number.optional(),
      discount : schema.number.optional(),
    })

    try {
      const {coupon_id} = params;
      const coupon = await Coupon.find(coupon_id);
      const payload = await request.validate({schema: coupon_validator});
      

      if (!coupon){
        return response.badRequest({message: 'No Coupon'});
      }

      if (payload.coupon_name){
        coupon.coupon_name = payload.coupon_name;
      }

      if (payload.coupon_limit_number){
        coupon.coupon_limit_number = payload.coupon_limit_number;
      }

      if (payload.coupon_limit_date){
        coupon.coupon_limit_date = payload.coupon_limit_date;
      }

      if (payload.counter || payload.counter == 0 ){
        coupon.counter = payload.counter;
      }

      if (payload.discount){
        coupon.discount = payload.discount;
      }

      await coupon.save();

      return response.ok({message: 'Coupon Updated', coupon: coupon})
    } catch (error) {
      return response.badRequest(error);
    }
  }

  public async destroy({response, params}: HttpContextContract) {
    try {
      const {coupon_id} = params;
      const coupon = await Coupon.find(coupon_id);

      if (!coupon){
        return response.badRequest({message: 'No Coupon'});
      }

      await coupon.delete();
      return response.ok({message: 'Coupon Deleted'});
    } catch (error) {
      return response.badRequest(error);
    }
  }

  public async check({request, response}: HttpContextContract){
    try {
      const {coupon_name} = request.body();

      if (!coupon_name){
        return response.badRequest({message: 'Coupon Name is needed'});
      }

      const coupon = await Coupon.findBy('coupon_name', coupon_name);

      if (!coupon){
        return response.badRequest({message: 'Coupon is not on database'});
      }

      if (coupon.coupon_limit_number){
        if (coupon.counter < coupon.coupon_limit_number){
          return response.ok({message: 'Coupon is Ready to use', discount : coupon.discount});
        }

        return response.badRequest({message: 'Coupon already at its limit'});
      }

      return response.ok({message: 'Coupon is Ready to Use', discount : coupon.discount});

    } catch (error) {
      return response.badRequest(error);
    }
  }

  public async useCoupon({request, response}: HttpContextContract){
    try {
      const {coupon_name} = request.body();

      if (!coupon_name){
        return response.badRequest({message: 'Coupon Name is needed'});
      }

      const coupon = await Coupon.findBy('coupon_name', coupon_name);

      if (!coupon){
        return response.badRequest({message: 'Coupon is not on database'});
      }

      if (coupon.coupon_limit_number){
        if (coupon.counter < coupon.coupon_limit_number){
          coupon.counter++;
          await coupon.save();
          return response.ok({message: 'Coupon is Ready to use', discount : coupon.discount});
        }

        return response.badRequest({message: 'Coupon already at its limit'});
      }

      return response.ok({message: 'Coupon is Ready to Use', discount : coupon.discount});

    } catch (error) {
      return response.badRequest(error);
    }
  }
}
