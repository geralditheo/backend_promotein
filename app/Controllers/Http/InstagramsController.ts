import type { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'

// Model
import Instagram from 'App/Models/Instagram';
import UserCreatorProfile from 'App/Models/UserCreatorProfile';

export default class InstagramsController {
  public async index({response}: HttpContextContract) {
    try {
      const instagram = await Instagram.all();

      if (instagram.length == 0){
        response.ok({message: 'Instagram Kosong'})
        return
      }

      response.ok({message: "Index Ok", data: instagram})
    } catch (error) {
      response.badRequest({message: error});
      console.log({message: error});
      
    }
  }

  public async store({request, response}: HttpContextContract) {
    try {
      const { nama_akun, spesialisasi_bidang, jumlah_follower, range_harga_story, range_harga_feed, range_harga_reels, user_creators_id,	bidang_kontens_id } = request.body();

      const id = user_creators_id;
      const profile = await UserCreatorProfile.findBy('user_creators_id', id);

      const instagram = new Instagram();

      instagram.nama_akun = nama_akun;
      instagram.spesialisasi_bidang = spesialisasi_bidang;
      instagram.jumlah_follower = jumlah_follower;
      instagram.range_harga_story = range_harga_story;
      instagram.range_harga_feed = range_harga_feed;
      instagram.range_harga_reels = range_harga_reels;

      instagram.bidang_kontens_id = bidang_kontens_id;

      if (profile){
        await profile.related('instagram').save(instagram);
      }
      
      if (!profile){
        response.ok({message: "Profile TIdak Tersedia"})
        return
      }
      
      response.ok({message: "Store Ok"})
    } catch (error) {
      response.badRequest({message: error});
      console.log({message: error});
      
    }
  }

  public async show({response, params}: HttpContextContract) {
    try {
      

      const user_creators_id = params.user_creators_id;

      const profile = await UserCreatorProfile.findBy('user_creators_id', user_creators_id);

      if (!profile){
        response.ok({message: "Profile Tidak Tersedia / belum dibuat "})
        return
      }
      
      const profiles_id = profile.id;

      const instagram = await Instagram.findBy('user_creator_profiles_id', profiles_id);

      if(!instagram){
        response.ok({message: "Instagram Tidak Tersedia / belum dibuat "})
        return
      }
      
    

      response.ok({message: "Show Ok", data: instagram})

    } catch (error) {
      response.badRequest({message: error});
      console.log({message: error});
      
    }
  }

  public async update({request, response, params}: HttpContextContract) {
    try {
      const user_creators_id = params.user_creators_id;
      const profile = await UserCreatorProfile.findBy('user_creators_id', user_creators_id);

      if (!profile){
        response.ok({message: "Profile Tidak Tersedia / belum dibuat "})
        return
      }

      const profiles_id = profile.id;

      const instagram = await Instagram.findBy('user_creator_profiles_id', profiles_id);

      if(!instagram){
        response.ok({message: "Instagram Tidak Tersedia / belum dibuat "})
        return
      }

      const { nama_akun, spesialisasi_bidang, jumlah_follower, range_harga_story, range_harga_feed, range_harga_reels, bidang_kontens_id } = request.body();

      if (nama_akun){
        instagram.nama_akun = nama_akun;
      }

      if (spesialisasi_bidang){
        instagram.spesialisasi_bidang = spesialisasi_bidang;
      }

      if (jumlah_follower){
        instagram.jumlah_follower = jumlah_follower;
      }

      if (range_harga_story){
        instagram.range_harga_story = range_harga_story;
      }

      if (range_harga_feed){
        instagram.range_harga_feed = range_harga_feed;
      }

      if (range_harga_reels){
        instagram.range_harga_reels = range_harga_reels;
      }

      if (bidang_kontens_id){
        instagram.bidang_kontens_id = bidang_kontens_id;
      }

      await profile.related('instagram').save(instagram);

      response.ok({message: "Show Ok"})

    } catch (error) {
      response.badRequest({message: error});
      console.log({message: error});
      
    }
  }

  public async destroy({response, params}: HttpContextContract) {
    try {
      const user_creators_id = params.user_creators_id;
      const profile = await UserCreatorProfile.findBy('user_creators_id', user_creators_id);

      if (!profile){
        response.ok({message: "Profile Tidak Tersedia / belum dibuat "})
        return
      }

      const profiles_id = profile.id;

      const instagram = await Instagram.findBy('user_creator_profiles_id', profiles_id);

      if(!instagram){
        response.ok({message: "Instagram Tidak Tersedia / belum dibuat "})
        return
      }

      await instagram.delete();

      response.ok({message: 'Instagram Deleted'})
    } catch (error) {
      response.badRequest({message: error});
      console.log({message: error});
      
    }
  }
}
