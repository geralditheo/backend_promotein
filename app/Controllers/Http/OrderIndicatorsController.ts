import type { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'

// Validator
import { schema } from '@ioc:Adonis/Core/Validator'

// Model
import OrderIndicator from 'App/Models/OrderIndicator'

export default class OrderIndicatorsController {
  // public async index({}: HttpContextContract) {}

  // public async create({}: HttpContextContract) {}

  public async store({request, response}: HttpContextContract) {
    const indicatorValidator = schema.create({
      number: schema.number.optional()
    })

    try {
      const payload = await request.validate({schema: indicatorValidator});

      if (!payload.number){
        payload.number = 1;
      }

      const order_indicator = new OrderIndicator();

      order_indicator.number = payload.number;

      await order_indicator.save();

      response.ok({messaeg: 'Ok'});

    } catch (error) {
      response.badRequest({message: error});
      console.log({message: error});
      
    }
  }

  // public async show({}: HttpContextContract) {}

  // public async edit({}: HttpContextContract) {}

  public async update({request, response, params}: HttpContextContract) {
    const indicatorValidator = schema.create({
      number: schema.number.optional()
    })

    try {
      const payload = await request.validate({schema: indicatorValidator});

      if (!payload.number){
        payload.number = 1;
      }

      const {indicator_id} = params;

      const order_indicator = await OrderIndicator.find(indicator_id);

      if (!order_indicator){
        response.ok({message: 'Tidak Tersedia'});
        return;
      }

      order_indicator.number = payload.number;

      await order_indicator.save();

      response.ok({message: 'Ok'});

    } catch (error) {
      response.badRequest({message: error});
      console.log({message: error});
      
    }
  }

  // public async destroy({}: HttpContextContract) {}
}
