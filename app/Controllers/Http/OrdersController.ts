import type { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'

// Schema
import { schema, rules } from '@ioc:Adonis/Core/Validator'

// Model
import Order from 'App/Models/Order';
import UserCreator from 'App/Models/UserCreator';
import UserMkm from 'App/Models/UserMkm';

export default class OrdersController {
  public async index({response}: HttpContextContract) {
    try {
      const order = await Order.all();

      if (order.length == 0){
        response.ok({message: 'Kosong'})
        return
      }

      response.ok({message: 'Ok', data: {order}})
    } catch (error) {
      response.badRequest({message: error})
      console.log({message: error});
      
    }
  }

  // public async create({}: HttpContextContract) {}

  public async store({request, response}: HttpContextContract) {
    const orderValidator =  schema.create({
        kunjungan_ke_lokasi: schema.string.optional({trim: true}),
        ide_promosi: schema.string.optional(),
        catatan: schema.string.optional(),
        deadline: schema.date.optional(),

        user_mkms_id: schema.number([
          rules.exists({
            table: 'user_mkms',
            column: 'id'
          })
        ]),
        user_creators_id: schema.number([
          rules.exists({
            table: 'user_creators',
            column: 'id'
          })
        ])
    })

    try {
      const payload = await request.validate({schema: orderValidator});

      const order = new Order();

      if (payload.kunjungan_ke_lokasi){
        order.kunjungan_ke_lokasi = String(payload.kunjungan_ke_lokasi);
      }

      if (payload.ide_promosi){
        order.ide_promosi = String(payload.ide_promosi);
      }

      if (payload.catatan){
        order.catatan = String(payload.catatan);
      }
      
      if (payload.deadline){
        order.deadline = payload.deadline;
      }
       
      order.user_creators_id = payload.user_creators_id;
      order.user_mkms_id = payload.user_mkms_id;

      await order.save();

      response.ok({message: 'Ok', data: {order, payload, }})

    } catch (error) {
      response.badRequest({message: error})
      console.log({message: error});
    }
  }

  public async show({response, params}: HttpContextContract) {
    try {
      const { order_id } = params;

      const order = await Order.query().preload('status').preload('produk').preload('user_mkm').where('id', order_id).first();

      if (!order){
        response.badRequest({message: 'Order Tidak Tersedia'});
        return
      }
      
      response.ok({message: "Ok", data: {order}});
    } catch (error) {
      response.badRequest({message: error})
      console.log({message: error});
    }
  }

  // public async edit({}: HttpContextContract) {}

  public async update({request, response, params}: HttpContextContract) {
    const orderValidator =  schema.create({
      kunjungan_ke_lokasi: schema.string.optional({trim: true}),
      ide_promosi: schema.string.optional(),
      catatan: schema.string.optional(),
      deadline: schema.date.optional(),

    })

    try {
      const payload = await request.validate({schema: orderValidator});
      const { order_id } = params;
      const order = await Order.find(order_id);
      
      if (!order){
        response.badRequest({message: 'Order Tidak Tersedia'});
        return
      }

      if (payload.kunjungan_ke_lokasi){
        order.kunjungan_ke_lokasi = String(payload.kunjungan_ke_lokasi);
      }

      if (payload.ide_promosi){
        order.ide_promosi = String(payload.ide_promosi);
      }

      if (payload.catatan){
        order.catatan = String(payload.catatan);
      }
      
      if (payload.deadline){
        order.deadline = payload.deadline;
      }

      await order.save();

      response.ok({message: 'Ok', data: {order, payload}});
       
    } catch (error) {
      response.badRequest({message: error})
      console.log({message: error});
    }
  }

  public async destroy({response, params}: HttpContextContract) {
    try {
      const { order_id } = params;

      const order = await Order.find(order_id);

      if (!order){
        response.badRequest({message: 'Order Tidak Tersedia'});
        return
      }

      await order.delete();
      
      response.ok({message: "Ok", });
    } catch (error) {
      response.badRequest({message: error})
      console.log({message: error});
    }
  }

  public async creator({response, params}: HttpContextContract){
    try {

      const { user_creators_id } = params;
      
      const user = await UserCreator.find(user_creators_id);

      if (!user){
        response.badRequest({message: 'User Tidak Tersedia'});
        return;
      }

      // const order = await user.related('order').query();
      // const order = await Order.query().preload('status', (statusQuery) =>  {
      //   statusQuery.where('isReviewed', 'yes')  
      // }).where('user_creators_id',user_creators_id );

      const order = await Order.query()
      .select('*')
      .from('orders')
      .join('statuses', 'orders.id', '=', 'statuses.order_id')
      .where('orders.user_creators_id', '=', user_creators_id)
      .where('statuses.isReviewed', '=', 'yes')
      .preload('status')
      .preload('user_mkm')
      .preload('produk');

      if (order.length == 0){
        response.ok({message: 'Order Kosong'});
        return;
      }
      
      response.ok({message: 'Ok', data: {user, order}});
    } catch (error) {
      response.badRequest({message: error});
      console.log({message: error});
    }
  }

  public async order_creator_accepted({response, params}: HttpContextContract){
    try {

      const { user_creators_id } = params;
      
      const user = await UserCreator.find(user_creators_id);

      if (!user){
        response.badRequest({message: 'User Tidak Tersedia'});
        return;
      }


      const order = await Order.query()
      .select('*')
      .from('orders')
      .join('statuses', 'orders.id', '=', 'statuses.order_id')
      .where('orders.user_creators_id', '=', user_creators_id)
      .where('statuses.isAccepted', '=', 'yes')
      .preload('status')
      .preload('user_mkm')
      .preload('produk');

      if (order.length == 0){
        response.ok({message: 'Order Kosong'});
        return;
      }
      
      response.ok({message: 'Ok', data: {user, order}});
    } catch (error) {
      response.badRequest({message: error});
      console.log({message: error});
    }
  }

  public async mkm({response, params}: HttpContextContract){
    try {

      const { user_mkms_id } = params;

      const user = await UserMkm.query().preload('order', (orderQuery) => {
        orderQuery.preload('status').preload('user_creator')
      }).where('id', user_mkms_id).first();

      if (!user){
        response.badRequest({message: 'User Tidak Tersedia'});
        return;
      }

      

      response.ok({message: 'Ok', data: {user}});
    } catch (error) {
      response.badRequest({message: error});
      console.log({message: error});
    }
  }
}
