import type { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'

// Validator
import { schema,rules } from '@ioc:Adonis/Core/Validator'

// Model
import PengajuanSaldo from 'App/Models/PengajuanSaldo';

export default class PengajuanSaldosController {
  public async index({response}: HttpContextContract) {
    try {
      const pengajuan_saldo = await PengajuanSaldo.all();

      if (pengajuan_saldo.length == 0){
        response.ok({message: 'Pengajuan Saldo Kosong'});
        return;
      }

      response.ok({message: 'Ok', data: {pengajuan_saldo}});

      response
    } catch (error) {
      response.badRequest({message: error});
      console.log({message: error});
    }
  }

  // public async create({}: HttpContextContract) {}

  public async store({request, response}: HttpContextContract) {

    const pengajuanSaldoValidator = schema.create({
      melalui: schema.enum(['bank', 'gopay', 'ovo', 'pulsa']),
      nomor_rekening: schema.string({trim: true}),
      status: schema.enum(['accepted', 'rejected', 'none']),

      user_creator_profiles_id: schema.number([
        rules.exists({
          table: 'user_creator_profiles',
          column: 'id'
        })
      ])
    })

    try {
      const paylaod = await request.validate({schema: pengajuanSaldoValidator});

      const pengajuan_saldo = new PengajuanSaldo();

      pengajuan_saldo.melalui = paylaod.melalui;
      pengajuan_saldo.nomor_rekening = paylaod.nomor_rekening;
      pengajuan_saldo.status = paylaod.status;

      pengajuan_saldo.user_creator_profiles_id = paylaod.user_creator_profiles_id;

      await pengajuan_saldo.save();

      response.ok({message: 'Ok', data: {pengajuan_saldo, paylaod}});

    } catch (error) {
      response.badRequest({message: error});
      console.log({message: error});
    }
  }

  public async show({response, params}: HttpContextContract) {
    try {
      const { user_creators_id } = params;

      const pengajuan_saldo = await PengajuanSaldo.findBy('user_creator_profiles_id', user_creators_id );

      if(!pengajuan_saldo){
        response.badRequest({message: 'Pengajuan Saldo Tidak Tersedia'});
        return;
      }

      response.ok({message: 'Ok', data: {pengajuan_saldo}});
    } catch (error) {
      response.badRequest({message: error});
      console.log({message: error});
    }
  }

  // public async edit({}: HttpContextContract) {}

  public async update({request, response, params}: HttpContextContract) {

    const pengajuanSaldoValidator = schema.create({
      melalui: schema.enum(['bank', 'gopay', 'ovo', 'pulsa']),
      nomor_rekening: schema.string({trim: true}),
      status: schema.enum(['accepted', 'rejected', 'none']),

      user_creator_profiles_id: schema.number([
        rules.exists({
          table: 'user_creator_profiles',
          column: 'id'
        })
      ])
    })

    try {
      
      const payload = await request.validate({schema: pengajuanSaldoValidator});

      const { user_creators_id } = params;

      const pengajuan_saldo = await PengajuanSaldo.findBy('user_creator_profiles_id', user_creators_id );

      if(!pengajuan_saldo){
        response.badRequest({message: 'Pengajuan Saldo Tidak Tersedia'});
        return;
      }

      pengajuan_saldo.melalui = payload.melalui;
      pengajuan_saldo.nomor_rekening = payload.nomor_rekening;
      pengajuan_saldo.status = payload.status;

      pengajuan_saldo.user_creator_profiles_id = payload.user_creator_profiles_id;

      await pengajuan_saldo.save();

      response.ok({message: 'Ok', data: {pengajuan_saldo}});

    } catch (error) {
      response.badRequest({message: error});
      console.log({message: error});
    }
  }

  public async destroy({response, params}: HttpContextContract) {
    try {
      const { user_creators_id } = params;

      const pengajuan_saldo = await PengajuanSaldo.findBy('user_creator_profiles_id', user_creators_id );

      if(!pengajuan_saldo){
        response.badRequest({message: 'Pengajuan Saldo Tidak Tersedia'});
        return;
      }

      await pengajuan_saldo.delete();

      response.ok({message: 'Ok'});
    } catch (error) {
      response.badRequest({message: error});
      console.log({message: error});
    }
  }
}
