import type { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'

// Validator
import { schema, rules } from '@ioc:Adonis/Core/Validator'

// Model
import PengirimanBarang from 'App/Models/PengirimanBarang';

export default class PengirimanBarangsController {
  public async index({response}: HttpContextContract) {
    try {
      const pengiriman_barang = await PengirimanBarang.all();

      if (pengiriman_barang.length == 0){
        response.ok({message: "Pengiriman Barang Kosong"});
        return;
      }

      response.ok({message: "Ok", data: {pengiriman_barang}});

    } catch (error) {
      response.badRequest({message: error});
      console.log({message: error});
      
    }
  }

  // public async create({}: HttpContextContract) {}

  public async store({request, response}: HttpContextContract) {

    const pengirimanBarangValidator = schema.create({
      nomor_resi: schema.string.optional({trim: true}),
      jenis_pengiriman: schema.string.optional({trim: true}),
      keadaan_barang: schema.string.optional({trim: true}),

      order_user_mkms_id: schema.number([
        rules.exists({
          table: 'orders',
          column: 'user_mkms_id'
        })
      ]),

      order_user_creators_id: schema.number([
        rules.exists({
          table: 'orders',
          column: 'user_creators_id'
        })
      ]),

      order_id: schema.number([
        rules.exists({
          table: 'orders',
          column: 'id'
        })
      ])

    }) 

    try {

      const payload = await request.validate({schema: pengirimanBarangValidator});

      const pengiriman_barang= new PengirimanBarang();

      if (payload.nomor_resi){
        pengiriman_barang.nomor_resi = payload.nomor_resi;
      }

      if (payload.jenis_pengiriman){
        pengiriman_barang.jenis_pengiriman = payload.jenis_pengiriman;
      }

      if (payload.keadaan_barang){
        pengiriman_barang.keadaan_barang = payload.keadaan_barang;
      }
      
      pengiriman_barang.order_user_mkms_id = payload.order_user_mkms_id;
      pengiriman_barang.order_user_creators_id = payload.order_user_creators_id;
      pengiriman_barang.order_id = payload.order_id;

      await pengiriman_barang.save();

      response.ok({message: "Ok", data: {payload, pengiriman_barang}});
      
    } catch (error) {
      response.badRequest({message: error});
      console.log({message: error});
    }
  }

  public async show({response, params}: HttpContextContract) {
    try {
      const {order_id} = params;

      const pengiriman_barang = await PengirimanBarang.findBy('order_id',order_id );

      if (!pengiriman_barang){
        response.badRequest({message: 'Pengiriman Barang Tidak Tersedia'});
        return;
      }

      response.ok({message: 'Ok', data: {pengiriman_barang}});
      
    } catch (error) {
      response.badRequest({message: error});
      console.log({message: error});
    }
  }

  // public async edit({}: HttpContextContract) {}

  public async update({request, response, params}: HttpContextContract) {

    const pengirimanBarangValidator = schema.create({
      nomor_resi: schema.string.optional({trim: true}),
      jenis_pengiriman: schema.string.optional({trim: true}),
      keadaan_barang: schema.string.optional({trim: true}),
    }) 

    try {
      const {order_id} = params;

      const pengiriman_barang = await PengirimanBarang.findBy('order_id',order_id );
      if (!pengiriman_barang){
        response.badRequest({message: 'Pengiriman Barang Tidak Tersedia'});
        return;
      }

      const payload = await request.validate({schema: pengirimanBarangValidator});
      if (payload.nomor_resi){
        pengiriman_barang.nomor_resi = payload.nomor_resi;
      }

      if (payload.jenis_pengiriman){
        pengiriman_barang.jenis_pengiriman = payload.jenis_pengiriman;
      }

      if (payload.keadaan_barang){
        pengiriman_barang.keadaan_barang = payload.keadaan_barang;
      }

      await pengiriman_barang.save();

      response.ok({message: 'Ok', data: {pengiriman_barang}});
      
    } catch (error) {
      response.badRequest({message: error});
      console.log({message: error});
    }
  }

  public async destroy({response, params}: HttpContextContract) {
    try {
      const {order_id} = params;

      const pengiriman_barang = await PengirimanBarang.findBy('order_id',order_id );

      if (!pengiriman_barang){
        response.badRequest({message: 'Pengiriman Barang Tidak Tersedia'});
        return;
      }

      await pengiriman_barang.delete();

      response.ok({message: 'Ok', });
      
    } catch (error) {
      response.badRequest({message: error});
      console.log({message: error});
    }
  }
}
