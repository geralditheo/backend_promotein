import type { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'

// Model
import PengajuanKela from 'App/Models/PengajuanKela'
import UserCreatorProfile from 'App/Models/UserCreatorProfile';

export default class PengjuanKelasController {
  public async index({response}: HttpContextContract) {
    try {

      const pengajuan_kelas = await PengajuanKela.all();

      if (pengajuan_kelas.length == 0){
        response.ok({message: 'Tidak Ada Pengajuan Saat Ini'})
        return
      }

      response.ok({message: "Index OK", data: pengajuan_kelas});
    } catch (error) {
      response.badRequest({message: error});
      console.log({message: error});
      
    }
  }

  // public async create({}: HttpContextContract) {}

  public async store({request, response}: HttpContextContract) {
    try {
      const { user_creators_id } = request.body() ;

      const profile = await UserCreatorProfile.findBy('user_creators_id', user_creators_id);

      if (!profile){
        response.ok({message: "Profile Tidak Tersedia / Belum dibuat"});
        return
      }

      const pengajuan = new PengajuanKela;

      // none wait accept accept reject
      pengajuan.status = 'wait';

      profile.related('pengajuan_kelas').save(pengajuan);
      
      response.ok({message: "Store OK"});
    } catch (error) {
      response.badRequest({message: error});
      console.log({message: error});
      
    }
  }

  public async show({response, params}: HttpContextContract) {
    try {
      const user_creators_id = params.user_creators_id;

      const profile = await UserCreatorProfile.findBy('user_creators_id', user_creators_id);

      if (!profile){
        response.ok({message: "Profile Tidak Tersedia / belum dibuat "})
        return
      }

      const profiles_id = profile.id;

      const pengajuan_kelas = await PengajuanKela.findBy('user_creator_profiles_id', profiles_id);

      if (!pengajuan_kelas){
        response.ok({message: "Pengajuan Tidak Tersedia / belum dibuat "})
        return
      }

      response.ok({message: "Show OK", data: pengajuan_kelas});
    } catch (error) {
      response.badRequest({message: error});
      console.log({message: error});
      
    }
  }

  // public async edit({}: HttpContextContract) {}

  public async update({request, response, params}: HttpContextContract) {
    try {
      const user_creators_id = params.user_creators_id;

      const profile = await UserCreatorProfile.findBy('user_creators_id', user_creators_id);

      if (!profile){
        response.ok({message: "Profile Tidak Tersedia / belum dibuat "})
        return
      }

      const profiles_id = profile.id;

      const pengajuan_kelas = await PengajuanKela.findBy('user_creator_profiles_id', profiles_id);

      if (!pengajuan_kelas){
        response.ok({message: "Pengajuan Tidak Tersedia / belum dibuat "})
        return
      }

      const { status } = request.body();

      if (status){
        pengajuan_kelas.status = status;
      }

      profile.related('pengajuan_kelas').save(pengajuan_kelas);


      response.ok({message: "Update OK"});
    } catch (error) {
      response.badRequest({message: error});
      console.log({message: error});
      
    }
  }

  public async destroy({response, params}: HttpContextContract) {
    try {

      const user_creators_id = params.user_creators_id;

      const profile = await UserCreatorProfile.findBy('user_creators_id', user_creators_id);

      if (!profile){
        response.ok({message: "Profile Tidak Tersedia / belum dibuat "})
        return
      }

      const profiles_id = profile.id;

      const pengajuan_kelas = await PengajuanKela.findBy('user_creator_profiles_id', profiles_id);

      if (!pengajuan_kelas){
        response.ok({message: "Pengajuan Tidak Tersedia / belum dibuat "})
        return
      }

      await pengajuan_kelas.delete()

      response.ok({message: "Destroy OK"});
    } catch (error) {
      response.badRequest({message: error});
      console.log({message: error});
      
    }
  }
}
