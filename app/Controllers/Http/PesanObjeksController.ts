import type { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'

// Validator
import { schema, rules } from '@ioc:Adonis/Core/Validator'

// Model
import PesanObjek from 'App/Models/PesanObjek'

export default class PesanObjeksController {
  public async index({response}: HttpContextContract) {
    try {

      const pesan_objek = await PesanObjek.all();

      if (pesan_objek.length == 0){
        response.ok({message: 'Pesan Objek Kosong'});
        return;
      }

      response.ok({message: 'Ok', data: {pesan_objek}});

    } catch (error) {
      response.badRequest({message: error});
      console.log({message: error});
      
    }
  }

  // public async create({}: HttpContextContract) {}

  public async store({request, response}: HttpContextContract) {

    const pesanObjekValidator = schema.create({
      tiktok: schema.enum(['true', 'false', 'none']),
      youtube_video: schema.enum(['true', 'false', 'none']),
      youtube_short: schema.enum(['true', 'false', 'none']),
      insta_story: schema.enum(['true', 'false', 'none']),
      insta_reels: schema.enum(['true', 'false', 'none']),
      insta_feed: schema.enum(['true', 'false', 'none']),
      all: schema.enum(['true', 'false', 'none']),

      order_id: schema.number([
        rules.exists({
          table: 'orders',
          column: 'id'
        })
      ])

    })

    try {
      const payload = await request.validate({schema: pesanObjekValidator});

      const pesan_objek = new PesanObjek();

      pesan_objek.tiktok = payload.tiktok;
      pesan_objek.youtube_short = payload.youtube_short;
      pesan_objek.youtube_video = payload.youtube_video;
      pesan_objek.insta_story = payload.insta_story;
      pesan_objek.insta_reels = payload.insta_reels;
      pesan_objek.insta_feed = payload.insta_feed;
      pesan_objek.all = payload.all;

      pesan_objek.order_id = payload.order_id;

      await pesan_objek.save();

      response.ok({message: 'Ok', data: { pesan_objek, payload}})
    } catch (error) {
      response.badRequest({message: error});
      console.log({message: error});
    }
  }

  public async show({response, params}: HttpContextContract) {
    try {
      const { objek_id } = params;

      const pesan_objek = await PesanObjek.find(objek_id);

      if (!pesan_objek){
        response.badRequest({message: 'Pesan Objek Tidak Tersedia'});
        return;
      }

      response.ok({message: 'Ok', data: {pesan_objek}});
    } catch (error) {
      response.badRequest({message: error});
      console.log({message: error});
    }
  }

  // public async edit({}: HttpContextContract) {}

  public async update({request, response, params}: HttpContextContract) {

    const pesanObjekValidator = schema.create({
      tiktok: schema.enum(['true', 'false', 'none']),
      youtube_video: schema.enum(['true', 'false', 'none']),
      youtube_short: schema.enum(['true', 'false', 'none']),
      insta_story: schema.enum(['true', 'false', 'none']),
      insta_reels: schema.enum(['true', 'false', 'none']),
      insta_feed: schema.enum(['true', 'false', 'none']),
      all: schema.enum(['true', 'false', 'none']),

      order_id: schema.number([
        rules.exists({
          table: 'orders',
          column: 'id'
        })
      ])

    })

    try {

      const payload = await request.validate({schema: pesanObjekValidator});

      const { objek_id } = params;

      const pesan_objek = await PesanObjek.find(objek_id);

      if (!pesan_objek){
        response.badRequest({message: 'Pesan Objek Tidak Tersedia'});
        return;
      }

      pesan_objek.tiktok = payload.tiktok;
      pesan_objek.youtube_short = payload.youtube_short;
      pesan_objek.youtube_video = payload.youtube_video;
      pesan_objek.insta_story = payload.insta_story;
      pesan_objek.insta_reels = payload.insta_reels;
      pesan_objek.insta_feed = payload.insta_feed;
      pesan_objek.all = payload.all;

      pesan_objek.order_id = payload.order_id;

      await pesan_objek.save();

      response.ok({message: 'Ok', data: { pesan_objek, payload}})
      
    } catch (error) {
      response.badRequest({message: error});
      console.log({message: error});
    }
  }

  public async destroy({response, params}: HttpContextContract) {
    try {
      
      const { objek_id } = params;

      const pesan_objek = await PesanObjek.find(objek_id);

      if (!pesan_objek){
        response.badRequest({message: 'Pesan Objek Tidak Tersedia'});
        return;
      }

      await pesan_objek.delete();
      
      response.ok({message: 'Ok'})
      
    } catch (error) {
      response.badRequest({message: error});
      console.log({message: error});
    }
  }
}
