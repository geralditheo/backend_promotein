import type { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'

// Validator
import { schema, rules } from '@ioc:Adonis/Core/Validator'

// Model
import Produk from 'App/Models/Produk'

// Drive
import Drive from '@ioc:Adonis/Core/Drive'

export default class ProduksController {
  public async index({response}: HttpContextContract) {
    try {
      const produk = await Produk.all();

      if (produk.length == 0){
        response.ok({message: 'Produk Kosong'});
        return;
      }

      response.ok({message: 'Ok', data: {produk}});

    } catch (error) {
      response.badRequest({message: error});
      console.log({message: error});
      

    }
  }

  // public async create({}: HttpContextContract) {}

  public async store({request, response}: HttpContextContract) {

    const produkValidator = schema.create({
      
      poto_produk: schema.file.optional({
        extnames: ['jpg', 'png', 'jpeg'],
      }),
      jenis_produk: schema.string.optional({trim: true}),
      deskripsi_produk: schema.string.optional({trim: true}),

      order_id: schema.number([
        rules.exists({
          table: 'orders',
          column: 'id'
        })
      ])

    });

    try {
      const payload = await request.validate({schema: produkValidator});
      
      console.log({payload: payload.poto_produk});

      const produk = new Produk();

      if (payload.poto_produk  ){
        await payload.poto_produk.moveToDisk('./images/order');
        const fileName = payload.poto_produk.fileName;
        

        if (fileName){
          produk.poto_produk = fileName;
        }
      }

      if (payload.deskripsi_produk){
        produk.deskripsi_produk = payload.deskripsi_produk;
      }

      if (payload.jenis_produk){
        produk.jenis_produk = payload.jenis_produk;
      }

      if (payload.order_id){
        produk.order_id = payload.order_id;
      }

      await produk.save();

      response.ok({message: 'Ok', data: {produk, payload} });

    } catch (error) {
      response.status(400).json({message: 'errorsss', error: error})
      console.log({message: 'error', error: error});

    }
  }

  public async show({response, params}: HttpContextContract) {
    try {
      const { produk_id } = params;
      const produk = await Produk.find(produk_id);

      if (!produk){
        response.badRequest({message: 'Produk Tidak Tersedia'});
        return;
      }
      const image = await Drive.getSignedUrl('images/order/' + produk.poto_produk);
      produk.poto_produk = image;

      response.ok({message: 'Ok', data: { produk }})
    } catch (error) {
      response.badRequest({message: error});
      console.log({message: error});
    }
  }

  // public async edit({}: HttpContextContract) {}

  public async update({request,response, params}: HttpContextContract) {

    const produkValidator = schema.create({
      poto_produk: schema.file.optional({
        extnames: ['jpg', 'png', 'jpeg'],
        size: '10mb'
      }),
      jenis_produk: schema.string.optional({trim: true}),
      deskripsi_produk: schema.string.optional({trim: true}),

      order_id: schema.number([
        rules.exists({
          table: 'orders',
          column: 'id'
        })
      ])

    });

    try {
      const { produk_id } = params;
      const produk = await Produk.find(produk_id);

      if (!produk){
        response.badRequest({message: 'Produk Tidak Tersedia'});
        return;
      }

      const payload = await request.validate({schema: produkValidator});

      if (payload.poto_produk){
        await payload.poto_produk.moveToDisk('./images/order');
        const fileName = payload.poto_produk.fileName;

        if (fileName){
          produk.poto_produk = fileName;
        }
      }

      if (payload.deskripsi_produk){
        produk.deskripsi_produk = payload.deskripsi_produk;
      }

      if (payload.jenis_produk){
        produk.jenis_produk = payload.jenis_produk;
      }

      if (payload.order_id){
        produk.order_id = payload.order_id;
      }
      
      await produk.save();

      response.ok({message: 'Ok', data: { produk }})
    } catch (error) {
      response.badRequest({message: error});
      console.log({message: error});
    }
  }

  public async destroy({response, params}: HttpContextContract) {
    try {
      const { produk_id } = params;
      const produk = await Produk.find(produk_id);

      if (!produk){
        response.badRequest({message: 'Produk Tidak Tersedia'});
        return;
      }
  
      await produk.delete();

      response.ok({message: 'Ok'})
    } catch (error) {
      response.badRequest({message: error});
      console.log({message: error});
    }
  }

  public async image({request, response, params}: HttpContextContract){
    try {
      const { produk_id } = params;
      const produk = await Produk.find(produk_id);

      if (!produk){
        response.badRequest({message: 'Produk Tidak Tersedia'});
        return;
      }

      const poto_produk = request.file('poto_produk');

      if (poto_produk){
        await poto_produk.moveToDisk('./images/order');
        const fileName = poto_produk.fileName;
        

        if (fileName){
          console.log(fileName);
        }
      }

      response.ok({message: 'This is from Images',image: poto_produk});
    } catch (error) {
      response.badRequest({message: error});
      console.log({message: error});
    }
  }
}
