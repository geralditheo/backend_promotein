import type { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'

// Validator
import { schema, rules } from '@ioc:Adonis/Core/Validator'

// Model
import Status from 'App/Models/Status'

export default class StatusesController {
  public async index({response}: HttpContextContract) {
    try {
      const status = await Status.all();
      if (status.length == 0 ){
        response.ok({message: 'Status Kosong'});
        return;
      }
      response.ok({message: 'Ok', data: {status}});

    } catch (error) {
      response.badRequest({message: error});
      console.log({message: error});
      
    }
  }

  // public async create({}: HttpContextContract) {}

  public async store({request, response}: HttpContextContract) {

    const statusValidator = schema.create({
      isReviewed: schema.enum(['yes', 'no']),
      isAccepted: schema.enum(['yes', 'no']),
      isRejected: schema.enum(['yes', 'no']),

      order_id: schema.number([
        rules.exists({
          table: 'orders',
          column: 'id'
        })
      ])

    })

    try {
      const payload = await request.validate({schema: statusValidator});

      const status = new Status();

      status.isReviewed = payload.isReviewed;
      status.isAccepted = payload.isAccepted;
      status.isRejected = payload.isRejected;

      status.order_id = payload.order_id;

      await status.save();
      response.ok({message: 'Ok', data: {status, payload}});
    } catch (error) {
      response.badRequest({message: error});
      console.log({message: error});
    }
  }

  public async show({response, params}: HttpContextContract) {
    try {
      
      const { status_id } = params;

      const status = await Status.find(status_id);

      if (!status){
        response.badRequest({message: 'Status Tidak Tersedia'});
        return;
      }

      response.ok({message: "Ok", data: {status}});
      
    } catch (error) {
      response.badRequest({message: error});
      console.log({message: error});
    }
  }

  // public async edit({}: HttpContextContract) {}

  public async update({request, response, params}: HttpContextContract) {
    const statusValidator = schema.create({
      isReviewed: schema.enum(['yes', 'no']),
      isAccepted: schema.enum(['yes', 'no']),
      isRejected: schema.enum(['yes', 'no']),

      order_id: schema.number([
        rules.exists({
          table: 'orders',
          column: 'id'
        })
      ])

    })
    
    try {
      
      const { status_id } = params;

      const status = await Status.find(status_id);

      if (!status){
        response.badRequest({message: 'Status Tidak Tersedia'});
        return;
      }

      const payload = await request.validate({schema: statusValidator});

      status.isReviewed = payload.isReviewed;
      status.isAccepted = payload.isAccepted;
      status.isRejected = payload.isRejected;

      status.order_id = payload.order_id;

      await status.save();

      response.ok({message: "Ok", data: {status}});
      
    } catch (error) {
      response.badRequest({message: error});
      console.log({message: error});
    }
  }

  public async destroy({response, params}: HttpContextContract) {
    try {
      const { status_id } = params;

      const status = await Status.find(status_id);

      if (!status){
        response.badRequest({message: 'Status Tidak Tersedia'});
        return;
      }

      await status.delete();

      response.ok({message: "Ok" });

    } catch (error) {
      response.badRequest({message: error});
      console.log({message: error});
    }
  }
}
