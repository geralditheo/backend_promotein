import type { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'

// Model
import Tiktok from 'App/Models/Tiktok';
import UserCreatorProfile from 'App/Models/UserCreatorProfile';

export default class TiktoksController {
  public async index({response}: HttpContextContract) {
    try {

      const tiktok = await Tiktok.all();

      if (tiktok.length == 0){
        response.ok({message: 'Tiktok Kosong'})
        return;
      }

      response.ok({message: "Index Ok", data: tiktok})
    } catch (error) {
      response.badRequest({message: error});
      console.log({message: error});
      
    }
  }

  // public async create({}: HttpContextContract) {}

  public async store({request, response}: HttpContextContract) {
    try {
      const { user_creators_id } = request.body();
      const { nama_akun, spesialisasi_bidang, jumlah_follower, range_harga_video,  bidang_kontens_id } = request.body();

      const profile = await UserCreatorProfile.findBy('user_creators_id',user_creators_id );

      if (!profile){
        response.ok({message: "Profile pada Tiktok Ini TIdak Tersedia"});
        return;
      }

      const tiktok = new Tiktok();

      tiktok.nama_akun = nama_akun;
      tiktok.spesialisasi_bidang = spesialisasi_bidang;
      tiktok.jumlah_follower = jumlah_follower;
      tiktok.range_harga_video = range_harga_video;
      tiktok.bidang_kontens_id = bidang_kontens_id;

      profile.related('tiktok').save(tiktok);

      response.ok({message: "Store Ok"})
    } catch (error) {
      response.badRequest({message: error});
      console.log({message: error});
      
    }
  }

  public async show({response, params}: HttpContextContract) {
    try {

      const user_creators_id = params.user_creators_id;

      const profile = await UserCreatorProfile.findBy('user_creators_id', user_creators_id);

      if (!profile){
        response.ok({message: "Profile Tidak Tersedia / belum dibuat "})
        return
      }

      const profiles_id = profile.id;

      const tiktok = await Tiktok.findBy('user_creator_profiles_id', profiles_id);

      if (!tiktok){
        response.ok({message: "Tiktok Tidak Tersedia / belum dibuat "})
        return
      }

      response.ok({message: "Show Ok", data: tiktok})
    } catch (error) {
      response.badRequest({message: error});
      console.log({message: error});
      
    }
  }

  // public async edit({}: HttpContextContract) {}

  public async update({request,response,params}: HttpContextContract) {
    try {

      const user_creators_id = params.user_creators_id;
      const profile = await UserCreatorProfile.findBy('user_creators_id', user_creators_id);

      if (!profile){
        response.ok({message: "Profile Tidak Tersedia / belum dibuat "})
        return
      }

      const profiles_id = profile.id;

      const tiktok = await Tiktok.findBy('user_creator_profiles_id', profiles_id);

      if(!tiktok){
        response.ok({message: "Tiktok Tidak Tersedia / belum dibuat "})
        return
      }

      const { nama_akun, spesialisasi_bidang, jumlah_follower, range_harga_video,  bidang_kontens_id } = request.body();

      if (nama_akun){
        tiktok.nama_akun = nama_akun;
      }

      if (spesialisasi_bidang){
        tiktok.spesialisasi_bidang = spesialisasi_bidang;
      }

      if (jumlah_follower){
        tiktok.jumlah_follower = jumlah_follower;
      }

      if (range_harga_video){
        tiktok.range_harga_video = range_harga_video;
      }

      
      if (bidang_kontens_id){
        tiktok.bidang_kontens_id = bidang_kontens_id;
      }

      profile.related('tiktok').save(tiktok);

      response.ok({message: "Update Ok"})
    } catch (error) {
      response.badRequest({message: error});
      console.log({message: error});
      
    }
  }

  public async destroy({response, params}: HttpContextContract) {
    try {
      const user_creators_id = params.user_creators_id;
      const profile = await UserCreatorProfile.findBy('user_creators_id', user_creators_id);

      if (!profile){
        response.ok({message: "Profile Tidak Tersedia / belum dibuat "})
        return
      }

      const profiles_id = profile.id;

      const tiktok = await Tiktok.findBy('user_creator_profiles_id', profiles_id);

      if(!tiktok){
        response.ok({message: "Tiktok Tidak Tersedia / belum dibuat "})
        return
      }

      await tiktok.delete();

      response.ok({message: "Destroy Ok"})
    } catch (error) {
      response.badRequest({message: error});
      console.log({message: error});
      
    }
  }
}
