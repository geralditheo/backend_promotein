import type { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'

// Validator
import { schema, rules } from '@ioc:Adonis/Core/Validator'

// Model
import Transaksi from 'App/Models/Transaksi'

export default class TransaksisController {
  public async index({response}: HttpContextContract) {
    try {
      const transaksi = await Transaksi.all()

      if (transaksi.length == 0){
        response.ok({message: 'Transaksi Kosong'});
        return;
      }

      response.ok({message: 'Ok', data: { transaksi }});
    } catch (error) {
      response.badRequest({message: error});
      console.log({message: error});
    }
  }

  // public async create({}: HttpContextContract) {}

  public async store({request, response}: HttpContextContract) {

    const transaksiValidator = schema.create({
      total_bayar: schema.number.optional(),
      poto_transaksi: schema.file.optional({
        extnames: ['jpg', 'png', 'jpeg']
      }),
      tanggal_dibayar: schema.date.optional(),

      order_id: schema.number([
        rules.exists({
          table: 'orders',
          column: 'id'
        })
      ])

    })

    try {

      const payload = await request.validate({schema: transaksiValidator});

      const transaksi = new Transaksi();

      if (payload.poto_transaksi){
        await payload.poto_transaksi.moveToDisk('./images/transaksi');
        
        const fileName = payload.poto_transaksi.fileName;

        if (fileName){
          transaksi.poto_transaksi = fileName;
        }

      }
      
      if (payload.total_bayar){
        transaksi.total_bayar = payload.total_bayar;
      }

      if (payload.tanggal_dibayar){
        transaksi.tanggal_dibayar = payload.tanggal_dibayar;
      }

      transaksi.order_id = payload.order_id;

      await transaksi.save();

      response.ok({message: "Ok", data: {transaksi, payload}});
    } catch (error) {
      response.badRequest({message: error});
      console.log({message: error});
    }
  }

  public async show({response, params}: HttpContextContract) {
    try {
      const { transaksi_id } = params;

      const transaksi = await Transaksi.find(transaksi_id);

      if (!transaksi){
        response.badRequest({message: 'Transaksi Tidak Tersedia'});
        return;
      }

      response.ok({message: 'Ok', data: {transaksi}});

    } catch (error) {
      response.badRequest({message: error});
      console.log({message: error});
    }
  }

  // public async edit({}: HttpContextContract) {}

  public async update({request, response, params}: HttpContextContract) {

    const transaksiValidator = schema.create({
      total_bayar: schema.number.optional(),
      poto_transaksi: schema.file.optional({
        extnames: ['jpg', 'png', 'jpeg']
      }),
      tanggal_dibayar: schema.date.optional(),

    })

    try {
      const payload = await request.validate({schema: transaksiValidator});

      const { transaksi_id } = params;

      const transaksi = await Transaksi.find(transaksi_id);

      if (!transaksi){
        response.badRequest({message: 'Transaksi Tidak Tersedia'});
        return;
      }

      if (payload.poto_transaksi){
        await payload.poto_transaksi.moveToDisk('./images/transaksi');
        
        const fileName = payload.poto_transaksi.fileName;

        if (fileName){
          transaksi.poto_transaksi = fileName;
        }

      }
      
      if (payload.total_bayar){
        transaksi.total_bayar = payload.total_bayar;
      }

      if (payload.tanggal_dibayar){
        transaksi.tanggal_dibayar = payload.tanggal_dibayar;
      }

      await transaksi.save();

      response.ok({message: "Ok", data: {transaksi, payload}});
      
    } catch (error) {
      response.badRequest({message: error});
      console.log({message: error});
    }
  }

  public async destroy({response, params}: HttpContextContract) {
    try {
      const { transaksi_id } = params;

      const transaksi = await Transaksi.find(transaksi_id);

      if (!transaksi){
        response.badRequest({message: 'Transaksi Tidak Tersedia'});
        return;
      }

      await transaksi.delete();

      response.ok({message: 'Ok', });

    } catch (error) {
      response.badRequest({message: error});
      console.log({message: error});
    }
  }
}
