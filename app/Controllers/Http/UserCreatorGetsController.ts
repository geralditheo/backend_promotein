import type { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'

// DriveFake
import Drive from '@ioc:Adonis/Core/Drive';

// Model
import UserCreator from 'App/Models/UserCreator';
import UserCreatorProfile from 'App/Models/UserCreatorProfile';
import Instagram from 'App/Models/Instagram';
import Youtube from 'App/Models/Youtube';

export default class UserCreatorGetsController {
  public async index({response}: HttpContextContract) {
      const users = await UserCreator.all();

      if (users.length == 0){
        response.ok({message: 'User Kososng'})
        return
      }

      response.ok({message: 'Ok', data: {users}});
  }

  public async limit({response}: HttpContextContract){
    const users = await UserCreator.all();

      if (users.length == 0){
        response.ok({message: 'User Kososng'})
        return
      }

      response.ok({message: 'Ok', data: {users}});
  }

  public async profile({request, response}: HttpContextContract){
    try {
      const {id} = request.params();

      const profile = await UserCreatorProfile.findBy('user_creators_id', id);
      
      if (!profile){
        response.badRequest({message: 'Profile TIdak Tersedia'});
        return;
      }

      response.ok({message: 'Ok show', data: { profile }});

    } catch (error) {
      response.badRequest({message: error});
      console.log({message: error});
      
    }
  }

  public async instagram({request, response}: HttpContextContract){
    try {
      const {id} = request.params();

      const youtube = await Youtube.findBy('user_creator_profiles_id', id);
      
      if (!youtube){
        response.badRequest({message: 'Youtube TIdak Tersedia'});
        return;
      }

      response.ok({message: 'Ok show', data: { youtube }});

    } catch (error) {
      response.badRequest({message: error});
      console.log({message: error});
      
    }
  }

  public async youtube({request, response}: HttpContextContract){
    try {
      const {id} = request.params();

      const instagram = await Instagram.findBy('user_creator_profiles_id', id);
      
      if (!instagram){
        response.badRequest({message: 'Instagram TIdak Tersedia'});
        return;
      }

      response.ok({message: 'Ok show', data: { instagram }});

    } catch (error) {
      response.badRequest({message: error});
      console.log({message: error});
      
    }
  }

  // public async create({}: HttpContextContract) {}

  // public async store({}: HttpContextContract) {}

  public async image({request, response}: HttpContextContract){
    try {
      const { imageName } = request.body();


      const url = await Drive.getUrl(`images/creator/${imageName}`);

      if (!url){
        response.badRequest({message: 'Gambar Tidak Tersedia'});
        return;
      }

      response.ok({message: 'Gambar', url});

    } catch (error) {
      response.badRequest({message: 'Error', error});
      console.log({message: 'Error', error});
      
    }
  }

  public async show({request, response}: HttpContextContract) {
    try {
      const {id} = request.params();

      const user = await UserCreator.query().preload('profile', (profileQuery) => {
        profileQuery.preload('instagram').preload('tiktok').preload('youtube');
      }).select('id', 'username', 'email').where('id', id).first()
      
      if (!user){
        response.badRequest({message: 'User TIdak Tersedia'});
        return;
      }

      response.ok({message: 'Ok show', data: { user }});

    } catch (error) {
      response.badRequest({message: error});
      console.log({message: error});
      
    }

  }

  public async priceAll({request, response}: HttpContextContract){
    try {
      const {id} = request.params();

      const user = await UserCreator.query().preload('profile', (profileQuery) => {
        profileQuery.preload('instagram', (instagramQuery) => {
          instagramQuery.select('range_harga_story', 'range_harga_feed', 'range_harga_reels')
        }).preload('tiktok', (tiktokQuery) => {
          tiktokQuery.select('range_harga_video')
        }).preload('youtube', (youtubeQuery) => {
          youtubeQuery.select('range_harga_video', 'range_harga_short')
        }).select('id');
      })
      .select('id', 'username', 'email').where('id', id).first();
      
      if (!user){
        response.badRequest({message: 'User Tidak Tersedia'});
      }
      
      response.ok({message: 'Price Ok', user: user});
      
    } catch (error) {
      response.badRequest({message: error});
      console.log({message: error});
      
    }
  }

  // public async edit({}: HttpContextContract) {}

  // public async update({}: HttpContextContract) {}

  // public async destroy({}: HttpContextContract) {}
}
