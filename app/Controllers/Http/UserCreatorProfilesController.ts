import type { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'

// Model
import UserCreatorProfile from 'App/Models/UserCreatorProfile';

// Application
import Application from '@ioc:Adonis/Core/Application'

// Drive
import Drive from '@ioc:Adonis/Core/Drive'

export default class UserCreatorProfilesController {

    public async index({response}: HttpContextContract){

        try {
            const profiles = await UserCreatorProfile.all();

            if (profiles.length == 0){
                response.badRequest({message: 'Profile Kosong'})
                return
            }

            console.log({profiles});

            response.ok({message: 'Index Ok', data: profiles})
        } catch (error) {
            response.badRequest({message: error});
            console.log({message: error});
        }

    }

    public async store({request, response}: HttpContextContract){

        try {
            
            const profile = new UserCreatorProfile();

            const {alamat,nomor_telepoon,domisili, deskripsi,user_creators_id } = request.body()
            const poto_profil = request.file('poto_profil')

            if (poto_profil) {
                await poto_profil.moveToDisk('./images/creator/')
                const fileName = poto_profil.fileName;
                
                profile.poto_profil = String(fileName);
            }
            
            console.log({alamat,nomor_telepoon,domisili, deskripsi,user_creators_id});
            

            // profile.poto_profil = poto_profil;
            profile.alamat = alamat;
            profile.nomor_telepoon = nomor_telepoon;
            profile.domisili = domisili;
            profile.deskripsi = deskripsi;
            profile.kelas = 'bronze';
            profile.user_creators_id = Number(user_creators_id) ;

            await profile.save();
            
            response.ok({message: 'Store Success', poto_profile: 'test'})

        } catch (error) {
            response.badRequest({message: error});
            console.log({message: error});
            
        }

       
    }

    public async show({response, params}: HttpContextContract){

        try {

            const user_creators_id = params.user_creators_id;
            const profile = await UserCreatorProfile.findBy('user_creators_id', Number(user_creators_id) );

            
            if(profile == null ){
                response.badRequest({message: `Tidak profile dengan id ${user_creators_id} ` })
                return
            }
            
            
            response.ok({message: ' Profile Tersedia Ok', profile: profile})
        } catch (error) {
            response.badRequest({message: error});
            console.log({message: error});
        }
        
       
    }

    public async update({ request, response, params}: HttpContextContract){
        try {
            const user_creators_id = params.user_creators_id;
            const profile = await UserCreatorProfile.findBy('user_creators_id', Number(user_creators_id) );

            const {alamat,nomor_telepoon,domisili, deskripsi} = request.body();
            const poto_profil = request.file('poto_profil');

            console.log({alamat,nomor_telepoon,domisili, deskripsi,user_creators_id, poto_profil});


            if (profile){

                if (poto_profil){
                    await poto_profil.moveToDisk('./images/creator/')
                    const fileName = poto_profil.fileName;
                
                    profile.poto_profil = String(fileName);
                }

                if (nomor_telepoon){
                    profile.nomor_telepoon = nomor_telepoon;
                }

                if (alamat){
                    profile.alamat = alamat;
                }

                if (domisili){
                    profile.domisili = domisili;
                }

                if (deskripsi){
                    profile.deskripsi = deskripsi
                }

                await profile.save();

            }
            

            console.log({profile});
            

            response.ok({message: 'Updated Success Ok'})
        } catch (error) {
            response.badRequest({message: error});
            console.log({message: error});
        }
       
    }

    public async destroy({response, params}: HttpContextContract){

        try {
            const user_creators_id = params.user_creators_id;
            const profile = await UserCreatorProfile.findBy('user_creators_id', Number(user_creators_id) );

            if (!profile){
                response.badRequest({message: 'profile tidak tersedia'})
                return
            }

            await profile.delete()

            response.ok({message: 'Destroyed Success Ok'})
        } catch (error) {
            response.badRequest({message: error});
            console.log({message: error});
        }

    }

}
