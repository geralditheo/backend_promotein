import type { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'

// Validator
import UserCreatorRegisterValidator from 'App/Validators/UserCreatorRegisterValidator';
import UserCreatorLoginValidator from 'App/Validators/UserCreatorLoginValidator';

// Model
import UserCreator from 'App/Models/UserCreator';
import UserCreatorToken from 'App/Models/UserCreatorToken';
import UserCreatorProfile from 'App/Models/UserCreatorProfile';

// Hash
import Hash from '@ioc:Adonis/Core/Hash'

// JSON Web Token
import jwt from 'jsonwebtoken'

// Env
import Env from '@ioc:Adonis/Core/Env'


export default class UserCreatorsController {
  public async register({request, response}: HttpContextContract){

    try {
      const payload = await request.validate(UserCreatorRegisterValidator);

      const user = new UserCreator();

      user.username = payload.username;
      user.email = payload.email;
      user.password = payload.password;
      
      await user.save();
      
      const profile = await user.related('profile').firstOrCreate({
        alamat: '',
        domisili: '',
        deskripsi: '',
        kelas: 'bronze',
        nomor_telepoon: '',
        poto_profil: ''
      })

     const instagram = await profile.related('instagram').firstOrCreate({
      nama_akun: '',
      jumlah_follower: 0,
      range_harga_feed: 0,
      range_harga_reels: 0,
      range_harga_story: 0,
      spesialisasi_bidang: '',
      bidang_kontens_id: 1
     })

     const youtube = await profile.related('youtube').firstOrCreate({
      nama_akun: '',
      jumlah_follower: 0,
      spesialisasi_bidang: '',
      bidang_kontens_id: 1,
      range_harga_short: 0,
      range_harga_video: 0,
     })

     const tiktok = await profile.related('tiktok').firstOrCreate({
       nama_akun: '',
       jumlah_follower: 0,
       spesialisasi_bidang: '',
       bidang_kontens_id: 1,
       range_harga_video: 0,
     })


      response.ok({message: "Register OK", payload: payload, user: user, profile: profile, instagram: instagram, youtube: youtube, tiktok: tiktok})
    } catch (error) {
      response.badRequest({message: error});
      console.log({message: error});
      
    }

  }

  public async login({request, response}: HttpContextContract){

    try {
      const payload = await request.validate(UserCreatorLoginValidator);

      const user = await UserCreator.findBy('email', payload.email);

      if (user) {
        console.log("User Tersedia");

        if (await Hash.verify( String(user?.password) ,String(payload.password) )) {
          // verified
          const user_token = new UserCreatorToken()
          
          const name = String(user?.username);
          const token = jwt.sign({id: user.id, name: user.username, email: user.email}, Env.get('JWT_SECRET', 'promoteinsecretlocal'), { expiresIn: '31d' } )

          user_token.name = name;
          user_token.type = 'api';
          user_token.token = token;

          await user?.related('token').save(user_token);
          
          console.log("Do some token");

          response.ok({messsage: 'Login Success', payload: {id: user.id, email: user.email, token: token, username: name } })

        }
        
      }else {
        response.badRequest({message: "User Tida Ada"})
      }


    } catch (error) {
      response.badRequest({message: error});
      console.log({message: error});
      
    }

  }

  public async logout({request, response}: HttpContextContract){
    try {
      
      const headers = request.headers();
      const authorization = headers.authorization!.split(' ', 2);
      const type = authorization![0];
      const token = authorization![1];

      const { email } = request.body();

      if (authorization && type == 'Bearer'){

        const isAnyToken = await UserCreatorToken.findBy('token', token);
        const verified = jwt.verify(token, Env.get('JWT_SECRET', 'promoteinsecretlocal'))

        if (verified && isAnyToken ){
          const user = await UserCreator.findBy('email', email );
          const user_token = await UserCreatorToken.query().where('user_creators_id', user!.id).delete();

          response.ok({message: "Logout Success"})    

        }else {
          response.unauthorized({message: "You are unauthorized"})
        }

      }else {
        response.unauthorized({message: "You are unauthorized"})
      }
      

    } catch (error) {
      response.badRequest({message: error});
      console.log({message: error});
    }
  }
}
