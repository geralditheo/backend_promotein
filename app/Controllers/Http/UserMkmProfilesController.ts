import type { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'

// Model
import UserMkm from 'App/Models/UserMkm'
import UserMkmProfile from 'App/Models/UserMkmProfile'


export default class UserMkmProfilesController {
  public async index({response}: HttpContextContract) {
    try {
      const profile = await UserMkmProfile.all();

      if (profile.length == 0){
        response.ok({message: 'Profile Kosong'})
        return
      }


      response.ok({message: 'Ok', data: profile});
    } catch (error) {
      response.badRequest({message: error})
      console.log({message: error});
      
    }
  }

  // public async create({}: HttpContextContract) {}

  public async store({request, response}: HttpContextContract) {
    try {
      const {user_mkms_id} = request.body();

      if (!user_mkms_id){
        response.badRequest({message: 'user_mkms_id harus diisi!'});
        return
      }

      const user = await UserMkm.findBy('id', user_mkms_id);

      if (!user){
        response.badRequest({message: 'User Tidak Tersedia atau belum dibuat'})
        return
      }

      const { jenis_usaha, target_marketing, no_telepon, alamat} = request.body();

      const poto_profile = request.file('poto_profile');

      const profile = new UserMkmProfile();

      if (poto_profile){
        await poto_profile.moveToDisk('./images/mkm/');

        const fileName = poto_profile.fileName;
        profile.poto_profile = String(fileName);
      }
      
      profile.jenis_usaha = jenis_usaha;
      profile.target_marketing = target_marketing;
      profile.no_telepon = no_telepon;
      profile.alamat = alamat;

      await user.related('profile').save(profile);

      response.ok({message: 'Ok', data: {user, profile}});
    } catch (error) {
      response.badRequest({message: error})
      console.log({message: error});
      
    }
  }

  public async show({response, params}: HttpContextContract) {
    try {

      const user_mkms_id = Number(params.user_mkms_id)

      if (!user_mkms_id){
        response.badRequest({message: 'Isi Parameter user_mkms_id !'})
        return
      }

      const profile = await UserMkmProfile.findBy('user_mkms_id', user_mkms_id);

      if (!profile){
        response.badRequest({message: 'Profile Tidak Tersedia'})
        return
      }

      response.ok({message: 'Ok', data: {profile}})
    } catch (error) {
      response.badRequest({message: error})
      console.log({message: error});
      
    }
  }

  // public async edit({}: HttpContextContract) {}

  public async update({request, response, params}: HttpContextContract) {
    try {
      const user_mkms_id = Number(params.user_mkms_id)

      if (!user_mkms_id){
        response.badRequest({message: 'Isi Parameter user_mkms_id !'})
        return
      }

      const profile = await UserMkmProfile.findBy('user_mkms_id', user_mkms_id);

      if (!profile){
        response.badRequest({message: 'Profile Tidak Tersedia'})
        return
      }

      const { jenis_usaha, target_marketing, no_telepon, alamat} = request.body();

      const poto_profile = request.file('poto_profile');

      if (poto_profile){
        await poto_profile.moveToDisk('./images/mkm/');

        const fileName = poto_profile.fileName;
        profile.poto_profile = String(fileName);
      }

      if (jenis_usaha){
        profile.jenis_usaha = jenis_usaha;
      }

      if (target_marketing){
        profile.target_marketing = target_marketing;
      }

      if (no_telepon){
        profile.no_telepon = no_telepon;
      }

      if (alamat){
        profile.alamat = alamat;
      }


      await profile.save();

      response.ok({message: 'Ok', data: {profile}})
    } catch (error) {
      response.badRequest({message: error})
      console.log({message: error});
      
    }
  }

  public async destroy({response, params}: HttpContextContract) {
    try {
      const user_mkms_id = Number(params.user_mkms_id)

      if (!user_mkms_id){
        response.badRequest({message: 'Isi Parameter user_mkms_id !'})
        return
      }

      const profile = await UserMkmProfile.findBy('user_mkms_id', user_mkms_id);

      if (!profile){
        response.badRequest({message: 'Profile Tidak Tersedia'})
        return
      }

      await profile.delete();

      response.ok({message: 'Ok'})
    } catch (error) {
      response.badRequest({message: error})
      console.log({message: error});
      
    }
  }
}
