import type { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'

// Model
import UserMkm from 'App/Models/UserMkm';
import UserMkmToken from 'App/Models/UserMkmToken';

// Validator
import UserMkmRegisterValidator from 'App/Validators/UserMkmRegisterValidator';
import UserMkmLoginValidator from 'App/Validators/UserMkmLoginValidator';

// Hash
import Hash from '@ioc:Adonis/Core/Hash'

// JSON Web Token
import jwt from 'jsonwebtoken'

// Env
import Env from '@ioc:Adonis/Core/Env'


export default class UserMkmsController {
    public async register({request, response}: HttpContextContract ){
        try {
            const payload = await request.validate(UserMkmRegisterValidator);

            const user = new UserMkm();

            user.username = payload.username;
            user.password = payload.password;
            user.email = payload.email;

            await user.save();

            response.ok({message: 'Register Succes', user: user});

        } catch (error) {
            response.badRequest({message: error});
            console.log({message: error});
        }
    }

    public async login({request, response} : HttpContextContract){
        try {
            const payload = await request.validate(UserMkmLoginValidator);
            const user = await UserMkm.findBy('email', payload.email);

            if (user){
                console.log('User Tersedia');

                if (await Hash.verify( String(user.password), String(payload.password) ) ){
                    const user_token = new UserMkmToken();

                    const name = String(user.username);
                    const token = jwt.sign({id: user.id, name: user.username, email: user.email}, Env.get('JWT_SECRET', 'promoteinsecretlocal'), { expiresIn: '31d' } );

                    user_token.name = name;
                    user_token.type = 'api';
                    user_token.token = token;

                    await user.related('token').save(user_token);

                    console.log("Do some token");

                    response.ok({messsage: 'Login Success', payload: {id: user.id, email: user.email, token: token, username: name } })

                }

                
            }else {
                response.badRequest({message: 'User Tidak Tersedia'})
            }
        } catch (error) {
            response.badRequest({message: error});
            console.log({message: error});
        }
    }

    public async logout({request, response}: HttpContextContract){
        try {
      
            const headers = request.headers();
            const authorization = headers.authorization!.split(' ', 2);
            const type = authorization![0];
            const token = authorization![1];

            const { email } = request.body();
      
            if (authorization && type == 'Bearer'){
      
              const isAnyToken = await UserMkmToken.findBy('token', token);
              const verified = jwt.verify(token, Env.get('JWT_SECRET', 'promoteinsecretlocal'))
      
              if (verified && isAnyToken ){
                const user = await UserMkm.findBy('email', email );
                const user_token = await UserMkmToken.query().where('user_mkms_id', user!.id).delete();
      
                response.ok({message: "Logout Success"})    
      
              }else {
                response.unauthorized({message: "You are unauthorized"})
              }
      
            }else {
              response.unauthorized({message: "You are unauthorized"})
            }
            
      
          } catch (error) {
            response.badRequest({message: error});
            console.log({message: error});
          }
    }
}
