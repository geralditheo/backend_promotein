import type { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'

// Model
import Youtube from 'App/Models/Youtube';
import UserCreatorProfile from 'App/Models/UserCreatorProfile';

export default class YoutubesController {
  public async index({response}: HttpContextContract) {
    try {
      const youtube = await Youtube.all();

      if (youtube.length == 0){
        response.ok({message: 'Youtube Kosong'})
        return
      }

      response.ok({message: "OK", data: youtube})
    } catch (error) {
      response.badRequest({message: error});
      console.log({message: error});
      
    }
  }

  // public async create({}: HttpContextContract) {}

  public async store({request, response}: HttpContextContract) {
    try {

      const { user_creators_id } = request.body();
      const { nama_akun, spesialisasi_bidang, jumlah_follower, range_harga_video, range_harga_short, bidang_kontens_id } = request.body();

      const profile = await UserCreatorProfile.findBy('user_creators_id',user_creators_id );

      if (!profile){
        response.ok({message: "Profile pada Youtube Ini TIdak Tersedia"})
        return
      }

      const youtube = new Youtube();

      youtube.nama_akun = nama_akun;
      youtube.spesialisasi_bidang = spesialisasi_bidang;
      youtube.jumlah_follower = jumlah_follower;
      youtube.range_harga_video = range_harga_video;
      youtube.range_harga_short = range_harga_short;
      youtube.bidang_kontens_id = bidang_kontens_id;

      profile.related('youtube').save(youtube);
      

      response.ok({message: "OK"})
    } catch (error) {
      response.badRequest({message: error});
      console.log({message: error});
      
    }
  }

  public async show({response, params}: HttpContextContract) {
    try {

      const user_creators_id = params.user_creators_id;

      const profile = await UserCreatorProfile.findBy('user_creators_id', user_creators_id);

      if (!profile){
        response.ok({message: "Profile Tidak Tersedia / belum dibuat "})
        return
      }

      const profiles_id = profile.id;

      const youtube = await Youtube.findBy('user_creator_profiles_id', profiles_id);

      if (!youtube){
        response.ok({message: "Youtube Tidak Tersedia / belum dibuat "})
        return
      }

      response.ok({message: "OK", data: youtube})
    } catch (error) {
      response.badRequest({message: error});
      console.log({message: error});
      
    }
  }

  // public async edit({}: HttpContextContract) {}

  public async update({request, response, params}: HttpContextContract) {
    try {
      const user_creators_id = params.user_creators_id;
      const profile = await UserCreatorProfile.findBy('user_creators_id', user_creators_id);

      
      if (!profile){
        response.ok({message: "Profile Tidak Tersedia / belum dibuat "})
        return
      }

      const profiles_id = profile.id;

      const youtube = await Youtube.findBy('user_creator_profiles_id', profiles_id);

      if(!youtube){
        response.ok({message: "Youtube Tidak Tersedia / belum dibuat "})
        return
      }

      const { nama_akun, spesialisasi_bidang, jumlah_follower, range_harga_video, range_harga_short, bidang_kontens_id } = request.body();

      if (nama_akun){
        youtube.nama_akun = nama_akun;
      }

      if (spesialisasi_bidang){
        youtube.spesialisasi_bidang = spesialisasi_bidang;
      }

      if (jumlah_follower){
        youtube.jumlah_follower = jumlah_follower;
      }

      if (range_harga_video){
        youtube.range_harga_video = range_harga_video;
      }

      if (range_harga_short){
        youtube.range_harga_short = range_harga_short;
      }

      if (bidang_kontens_id){
        youtube.bidang_kontens_id = bidang_kontens_id;
      }

      profile.related('youtube').save(youtube);


      response.ok({message: "OK"})
    } catch (error) {
      response.badRequest({message: error});
      console.log({message: error});
      
    }
  }

  public async destroy({response, params}: HttpContextContract) {
    try {
      const user_creators_id = params.user_creators_id;
      const profile = await UserCreatorProfile.findBy('user_creators_id', user_creators_id);

      if (!profile){
        response.ok({message: "Profile Tidak Tersedia / belum dibuat "})
        return
      }

      const profiles_id = profile.id;

      const youtube = await Youtube.findBy('user_creator_profiles_id', profiles_id);

      if (!youtube){
        response.ok({message: "Profile Tidak Tersedia / belum dibuat "})
        return
      }

      await youtube.delete()

      response.ok({message: "OK"})
    } catch (error) {
      response.badRequest({message: error});
      console.log({message: error});      
    }
  }
}
