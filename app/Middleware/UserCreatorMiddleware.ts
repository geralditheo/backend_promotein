import type { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'

// Model
import UserCreator from 'App/Models/UserCreator';
import UserCreatorToken from 'App/Models/UserCreatorToken';

// JSON Web Token
import jwt from 'jsonwebtoken'

// Env
import Env from '@ioc:Adonis/Core/Env'

export default class UserCreatorMiddleware {
  public async handle({request, response}: HttpContextContract, next: () => Promise<void>) {
    // code for middleware goes here. ABOVE THE NEXT CALL

    try {

      const headers = request.headers();
      const authorization = headers.authorization!.split(' ');
      const type = authorization![0];
      const token = authorization![1];

      if (type != 'Bearer'){
        response.unauthorized({message: 'Please use Bearer Token'})

        return
      }
  
      if(authorization && type == 'Bearer' ){
        const isAnyToken = await UserCreatorToken.findBy('token', token );
        const verified = jwt.verify(token, Env.get('JWT_SECRET', 'promoteinsecretlocal'))

        if (isAnyToken && verified){
          console.log({isAnyToken, verified});
        }else {

          response.unauthorized({message: 'Unauthorized'})
          console.log({message: 'Unauthorized'});
    
          return    
        }
        
      }  

    } catch (error) {
      response.unauthorized({message: 'Unauthorized', error: error})
      console.log({message: 'Unauthorized', error: error});

      return
    }


    await next()
  }
}
