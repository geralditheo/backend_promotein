import { DateTime } from 'luxon'
import { BaseModel, column } from '@ioc:Adonis/Lucid/Orm'

export default class Coupon extends BaseModel {
  @column({ isPrimary: true })
  public id: number

  @column({columnName: 'coupon_name'})
  public coupon_name: string

  @column({columnName: 'coupon_limit_number'})
  public coupon_limit_number: number

  @column({columnName: 'coupon_limit_date'})
  public coupon_limit_date: DateTime

  @column({columnName: 'counter'})
  public counter: number

  @column({columnName: 'discount'})
  public discount: number

  @column.dateTime({ autoCreate: true })
  public createdAt: DateTime

  @column.dateTime({ autoCreate: true, autoUpdate: true })
  public updatedAt: DateTime
}
