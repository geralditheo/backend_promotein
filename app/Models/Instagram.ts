import { DateTime } from 'luxon'
import { BaseModel, column, belongsTo, BelongsTo } from '@ioc:Adonis/Lucid/Orm'

// Model
import UserCreatorProfile from 'App/Models/UserCreatorProfile';
import BidangKonten from 'App/Models/BidangKonten';

export default class Instagram extends BaseModel {
  @column({ isPrimary: true })
  public id: number

  @column({columnName: 'nama_akun'})
  public nama_akun: string

  @column({columnName: 'spesialisasi_bidang'})
  public spesialisasi_bidang: string

  @column({columnName: 'jumlah_follower'})
  public jumlah_follower: number


  @column({columnName: 'range_harga_story'})
  public range_harga_story: number

  @column({columnName: 'range_harga_feed'})
  public range_harga_feed: number

  @column({columnName: 'range_harga_reels'})
  public range_harga_reels: number


  @column({columnName: 'user_creator_profiles_id'})
  public user_creator_profiles_id: number

  @column({columnName: 'bidang_kontens_id'})
  public bidang_kontens_id: number

  
  @belongsTo(() => UserCreatorProfile, {
    foreignKey: 'user_creator_profiles_id'
  })
  public profile: BelongsTo<typeof UserCreatorProfile>

  @belongsTo(() => BidangKonten, {
    foreignKey: 'bidang_kontens_id'
  })
  public bidang: BelongsTo<typeof BidangKonten>


  @column.dateTime({ autoCreate: true })
  public createdAt: DateTime

  @column.dateTime({ autoCreate: true, autoUpdate: true })
  public updatedAt: DateTime
}
