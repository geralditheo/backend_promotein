import { DateTime } from 'luxon'
import { BaseModel, column, belongsTo, BelongsTo, hasOne, HasOne } from '@ioc:Adonis/Lucid/Orm'

// Model
import UserCreator from 'App/Models/UserCreator'
import UserMkm from 'App/Models/UserMkm'

import PengirimanBarang from 'App/Models/PengirimanBarang'
import Status from 'App/Models/Status'
import Produk from 'App/Models/Produk'
import Transaksi from 'App/Models/Transaksi'
import PesanObjek from 'App/Models/PesanObjek'

export default class Order extends BaseModel {
  @column({ isPrimary: true })
  public id: number

  @column({columnName: 'kunjungan_ke_lokasi'})
  public kunjungan_ke_lokasi: string

  @column({columnName: 'ide_promosi'})
  public ide_promosi: string

  @column({columnName: 'catatan'})
  public catatan: string

  @column.date({columnName: 'deadline'})
  public deadline: DateTime

  @column({columnName: 'user_mkms_id'})
  public user_mkms_id: number

  @column({columnName: 'user_creators_id'})
  public user_creators_id: number

  // Relation
  @belongsTo(() => UserMkm, {
    foreignKey: 'user_mkms_id'
  })
  public user_mkm: BelongsTo<typeof UserMkm>

  @belongsTo(() => UserCreator, {
    foreignKey: 'user_creators_id'
  })
  public user_creator: BelongsTo<typeof UserCreator>

  @hasOne(() => PengirimanBarang, {
    foreignKey: 'order_id'
  })
  public pengiriman_barang: HasOne<typeof PengirimanBarang>

  @hasOne(() => Status, {
    foreignKey: 'order_id'
  })
  public status: HasOne<typeof Status>

  @hasOne(() => Produk, {
    foreignKey: 'order_id'
  })
  public produk: HasOne<typeof Produk>

  @hasOne(() => Transaksi, {
    foreignKey: 'order_id'
  })
  public transaksi: HasOne<typeof Transaksi>

  @hasOne(() => PesanObjek, {
    foreignKey: 'order_id'
  })
  public objek: HasOne<typeof PesanObjek>

  @column.dateTime({ autoCreate: true })
  public createdAt: DateTime

  @column.dateTime({ autoCreate: true, autoUpdate: true })
  public updatedAt: DateTime
}
