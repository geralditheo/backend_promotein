import { DateTime } from 'luxon'
import { BaseModel, column, belongsTo, BelongsTo } from '@ioc:Adonis/Lucid/Orm'

// Model
import UserCreatorProfile from 'App/Models/UserCreatorProfile'

export default class PengajuanKela extends BaseModel {
  @column({ isPrimary: true })
  public id: number

  @column({ columnName: 'status' })
  public status: string

  @column({columnName: 'user_creator_profiles_id'})
  public user_creator_profiles_id: number


  @belongsTo(() => UserCreatorProfile, {
    foreignKey: 'user_creator_profiles_id'
  })
  public profile: BelongsTo<typeof UserCreatorProfile>
  

  @column.dateTime({ autoCreate: true })
  public createdAt: DateTime

  @column.dateTime({ autoCreate: true, autoUpdate: true })
  public updatedAt: DateTime
}
