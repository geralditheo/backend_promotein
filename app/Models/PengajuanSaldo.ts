import { DateTime } from 'luxon'
import { BaseModel, column, belongsTo, BelongsTo } from '@ioc:Adonis/Lucid/Orm'

// Model
import UserCreatorProfile from 'App/Models/UserCreatorProfile'

export default class PengajuanSaldo extends BaseModel {
  @column({ isPrimary: true })
  public id: number

  @column({columnName: 'melalui'})
  public melalui: string

  @column({columnName: 'nomor_rekening'})
  public nomor_rekening: string

  @column({columnName: 'status'})
  public status: string

  @column({columnName: 'user_creator_profiles_id'})
  public user_creator_profiles_id: number

  // Relation
  @belongsTo(() => UserCreatorProfile, {
    foreignKey: 'user_creator_profiles_id'
  })
  public user: BelongsTo<typeof UserCreatorProfile>

  
  @column.dateTime({ autoCreate: true })
  public createdAt: DateTime

  @column.dateTime({ autoCreate: true, autoUpdate: true })
  public updatedAt: DateTime
}
