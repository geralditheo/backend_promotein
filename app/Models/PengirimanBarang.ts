import { DateTime } from 'luxon'
import { BaseModel, column, belongsTo, BelongsTo } from '@ioc:Adonis/Lucid/Orm'

// Model
import Order from 'App/Models/Order'

export default class PengirimanBarang extends BaseModel {
  @column({ isPrimary: true })
  public id: number

  @column({columnName: 'nomor_resi'})
  public nomor_resi: string

  @column({columnName: 'jenis_pengiriman'})
  public jenis_pengiriman: string

  @column({columnName: 'keadaan_barang'})
  public keadaan_barang: string

  @column({columnName: 'order_user_mkms_id'})
  public order_user_mkms_id: number

  @column({columnName: 'order_user_creators_id'})
  public order_user_creators_id: number

  @column({columnName: 'order_id'})
  public order_id: number

  // Relation
  @belongsTo(() => Order, {
    foreignKey: 'order_id'
  })
  public order: BelongsTo<typeof Order>

  @belongsTo(() => Order, {
    foreignKey: 'order_user_creators_id'
  })
  public user_creator: BelongsTo<typeof Order>

  @belongsTo(() => Order, {
    foreignKey: 'order_user_mkms_id'
  })
  public user_mkm: BelongsTo<typeof Order>


  @column.dateTime({ autoCreate: true })
  public createdAt: DateTime

  @column.dateTime({ autoCreate: true, autoUpdate: true })
  public updatedAt: DateTime
}
