import { DateTime } from 'luxon'
import { BaseModel, column, belongsTo, BelongsTo } from '@ioc:Adonis/Lucid/Orm'

// Model
import Order from 'App/Models/Order'

export default class PesanObjek extends BaseModel {
  @column({ isPrimary: true })
  public id: number

  @column({columnName: 'tiktok'})
  public tiktok: string

  @column({columnName: 'youtube_video'})
  public youtube_video: string

  @column({columnName: 'youtube_short'})
  public youtube_short: string

  @column({columnName: 'insta_story'})
  public insta_story: string

  @column({columnName: 'insta_reels'})
  public insta_reels: string

  @column({columnName: 'insta_feed'})
  public insta_feed: string

  @column({columnName: 'all'})
  public all: string

  @column({columnName: 'order_id'})
  public order_id: number

  // Relation
  @belongsTo(() => Order, {
    foreignKey: 'order_id'
  })
  public order: BelongsTo<typeof Order>



  @column.dateTime({ autoCreate: true })
  public createdAt: DateTime

  @column.dateTime({ autoCreate: true, autoUpdate: true })
  public updatedAt: DateTime
}
