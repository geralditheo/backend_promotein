import { DateTime } from 'luxon'
import { BaseModel, column, belongsTo, BelongsTo } from '@ioc:Adonis/Lucid/Orm'

// Model
import Order from 'App/Models/Order'

export default class Produk extends BaseModel {
  @column({ isPrimary: true })
  public id: number

  @column({columnName: 'poto_produk'})
  public poto_produk: string

  @column({columnName: 'jenis_produk'})
  public jenis_produk: string

  @column({columnName: 'deskripsi_produk'})
  public deskripsi_produk: string

  @column({columnName: 'order_id'})
  public order_id: number

  // Relation
  @belongsTo(() => Order, {
    foreignKey: 'order_id'
  })
  public order : BelongsTo<typeof Order>


  @column.dateTime({ autoCreate: true })
  public createdAt: DateTime

  @column.dateTime({ autoCreate: true, autoUpdate: true })
  public updatedAt: DateTime
}
