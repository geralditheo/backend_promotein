import { DateTime } from 'luxon'
import { BaseModel, column, belongsTo, BelongsTo } from '@ioc:Adonis/Lucid/Orm'

// Model
import Order from 'App/Models/Order'

export default class Status extends BaseModel {
  @column({ isPrimary: true })
  public id: number

  @column({columnName: 'isReviewed'})
  public isReviewed: string

  @column({columnName: 'isAccepted'})
  public isAccepted: string

  @column({columnName: 'isRejected'})
  public isRejected: string

  @column({columnName: 'order_id'})
  public order_id: number

  // Relation
  @belongsTo(() => Order, {
    foreignKey: 'id'
  })
  public order: BelongsTo<typeof Order>


  @column.dateTime({ autoCreate: true })
  public createdAt: DateTime

  @column.dateTime({ autoCreate: true, autoUpdate: true })
  public updatedAt: DateTime
}
