import { DateTime } from 'luxon'
import { BaseModel, column, belongsTo, BelongsTo } from '@ioc:Adonis/Lucid/Orm'

// Order
import Order from 'App/Models/Order'

export default class Transaksi extends BaseModel {
  @column({ isPrimary: true })
  public id: number

  @column({columnName: 'total_bayar'})
  public total_bayar: number

  @column({columnName: 'poto_transaksi'})
  public poto_transaksi: string

  @column.dateTime({columnName: 'tanggal_dibayar'})
  public tanggal_dibayar: DateTime

  @column({columnName: 'order_id'})
  public order_id: number

  // Relation
  @belongsTo(() => Order, {
    foreignKey: 'order_id'
  })
  public order: BelongsTo<typeof Order>

  

  @column.dateTime({ autoCreate: true })
  public createdAt: DateTime

  @column.dateTime({ autoCreate: true, autoUpdate: true })
  public updatedAt: DateTime
}
