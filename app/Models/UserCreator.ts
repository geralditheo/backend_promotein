import { DateTime } from 'luxon'
import { BaseModel, column, beforeSave, hasOne, HasOne, hasMany, HasMany } from '@ioc:Adonis/Lucid/Orm'

// Hash
import Hash from '@ioc:Adonis/Core/Hash'

// Model
import UserCreatorToken from 'App/Models/UserCreatorToken'
import UserCreatorProfile from 'App/Models/UserCreatorProfile'
import Order from 'App/Models/Order'


export default class UserCreator extends BaseModel {
  @column({ isPrimary: true })
  public id: number

  @column({columnName: 'username'})
  public username: string

  @column({columnName: 'email'})
  public email: string

  @column({columnName: 'password'})
  public password: string

  @column.dateTime({ autoCreate: true })
  public createdAt: DateTime

  @column.dateTime({ autoCreate: true, autoUpdate: true })
  public updatedAt: DateTime

  @beforeSave()
  public static async hashPassword(user: UserCreator){
    if (user.$dirty.password){
      user.password = await Hash.make(user.password)
    }
  }


  // Relation
  @hasOne(() => UserCreatorToken, {
    foreignKey: 'user_creators_id'
  })
  public token: HasOne<typeof UserCreatorToken>

  @hasOne(() => UserCreatorProfile, {
    foreignKey: 'user_creators_id'
  })
  public profile: HasOne<typeof UserCreatorProfile>

  @hasMany(() => Order, {
    foreignKey: 'user_creators_id'
  })
  public order: HasMany<typeof Order>

}

UserCreator.$getRelation('profile').model
UserCreator.$getRelation('profile').relatedModel()