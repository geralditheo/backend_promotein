import { DateTime } from 'luxon'
import { BaseModel, column, belongsTo, BelongsTo, hasOne, HasOne } from '@ioc:Adonis/Lucid/Orm'

// Model
import UserCreator from 'App/Models/UserCreator'
import Instagram from 'App/Models/Instagram'
import Youtube  from 'App/Models/Youtube'
import Tiktok from 'App/Models/Tiktok'
import PengajuanKela from 'App/Models/PengajuanKela'
import PengajuanSaldo from 'App/Models/PengajuanSaldo'

export default class UserCreatorProfile extends BaseModel {
  @column({ isPrimary: true })
  public id: number

  @column({ columnName: 'poto_profil' })
  public poto_profil:string

  @column({columnName: 'kelas'})
  public kelas: string

  @column({columnName: 'nomor_telepoon'})
  public nomor_telepoon: string

  @column({columnName: 'alamat'})
  public alamat: string

  @column({columnName: 'domisili'})
  public domisili: string

  @column({columnName: 'deskripsi'})
  public deskripsi: string

  @column({columnName: 'user_creators_id'})
  public user_creators_id: number


  // Relationship
  @belongsTo(() => UserCreator, {
    foreignKey: 'user_creators_id'
  })
  public user: BelongsTo<typeof UserCreator>

  @hasOne(() => Instagram, {
    foreignKey: 'user_creator_profiles_id'
  })
  public instagram: HasOne<typeof Instagram>

  @hasOne(() => Youtube, {
    foreignKey: 'user_creator_profiles_id'
  })
  public youtube: HasOne<typeof Youtube>

  @hasOne(() => Tiktok, {
    foreignKey: 'user_creator_profiles_id'
  })
  public tiktok: HasOne<typeof Tiktok>

  @hasOne(() => PengajuanKela, {
    foreignKey: 'user_creator_profiles_id'
  })
  public pengajuan_kelas: HasOne<typeof PengajuanKela>

  @hasOne(() => PengajuanSaldo, {
    foreignKey: 'user_creator_profiles_id'
  })
  public pengajuan_saldo: HasOne<typeof PengajuanSaldo>


  // Date
  @column.dateTime({ autoCreate: true })
  public createdAt: DateTime

  @column.dateTime({ autoCreate: true, autoUpdate: true })
  public updatedAt: DateTime
}
