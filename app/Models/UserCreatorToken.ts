import { DateTime } from 'luxon'
import { BaseModel, column, belongsTo, BelongsTo } from '@ioc:Adonis/Lucid/Orm'

// Model
import UserCreator from 'App/Models/UserCreator'

export default class UserCreatorToken extends BaseModel {
  @column({ isPrimary: true })
  public id: number

  @column({columnName: 'name'})
  public name: string

  @column({columnName: 'type'})
  public type: string

  @column({columnName: 'token'})
  public token: string

  @column({columnName: 'user_creators_id'})
  public user_creators_id: number  

  @belongsTo(() => UserCreator, {
    foreignKey: 'user_creators_id'
  })
  public user: BelongsTo<typeof UserCreator>


  @column.dateTime({ autoCreate: true })
  public createdAt: DateTime

  @column.dateTime({ autoCreate: true, autoUpdate: true })
  public updatedAt: DateTime
}
