import { DateTime } from 'luxon'
import { BaseModel, column, beforeSave, hasOne, HasOne, hasMany, HasMany } from '@ioc:Adonis/Lucid/Orm'

// Hash
import Hash from '@ioc:Adonis/Core/Hash'

// Model
import UserMkmToken from 'App/Models/UserMkmToken'
import UserMkmProfile from 'App/Models/UserMkmProfile'
import Order from 'App/Models/Order'

export default class UserMkm extends BaseModel {
  @column({ isPrimary: true })
  public id: number

  @column({columnName: 'username'})
  public username: string

  @column({columnName: 'email'})
  public email: string

  @column({columnName: 'password'})
  public password: string

  @beforeSave()
  public static async hashPassword(user: UserMkm){
    if (user.$dirty.password){
      user.password = await Hash.make(user.password)
    }
  }

  // Relation
  @hasOne(() => UserMkmToken, {
    foreignKey: 'user_mkms_id'
  })
  public token: HasOne<typeof UserMkmToken>

  @hasOne(() => UserMkmProfile, {
    foreignKey: 'user_mkms_id'
  })
  public profile: HasOne<typeof UserMkmProfile>

  @hasMany(() => Order, {
    foreignKey: 'user_mkms_id'
  })
  public order: HasMany<typeof Order>

  @column.dateTime({ autoCreate: true })
  public createdAt: DateTime

  @column.dateTime({ autoCreate: true, autoUpdate: true })
  public updatedAt: DateTime
}
