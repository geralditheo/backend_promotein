import { DateTime } from 'luxon'
import { BaseModel, column, belongsTo, BelongsTo } from '@ioc:Adonis/Lucid/Orm'

// Model
import UserMkm from 'App/Models/UserMkm'

export default class UserMkmProfile extends BaseModel {
  @column({ isPrimary: true })
  public id: number

  @column({columnName: 'poto_profile'})
  public poto_profile: string

  @column({columnName: 'jenis_usaha'})
  public jenis_usaha: string

  @column({columnName: 'target_marketing'})
  public target_marketing: string

  @column({columnName: 'no_telepon'})
  public no_telepon: string

  @column({columnName: 'alamat'})
  public alamat: string

  @column({columnName: 'user_mkms_id'})
  public user_mkms_id: number

  // Relation
  @belongsTo(() => UserMkm, {
    foreignKey: 'user_mkms_id'
  })
  public user: BelongsTo<typeof UserMkm>

  @column.dateTime({ autoCreate: true })
  public createdAt: DateTime

  @column.dateTime({ autoCreate: true, autoUpdate: true })
  public updatedAt: DateTime
}
