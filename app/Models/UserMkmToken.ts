import { DateTime } from 'luxon'
import { BaseModel, column, belongsTo, BelongsTo } from '@ioc:Adonis/Lucid/Orm'

// Model
import UserMkm from 'App/Models/UserMkm'

export default class UserMkmToken extends BaseModel {
  @column({ isPrimary: true })
  public id: number

  @column({columnName: 'name'})
  public name: string

  @column({columnName: 'type'})
  public type: string

  @column({columnName: 'token'})
  public token: string

  @column({columnName: 'user_mkms_id'})
  public user_mkms_id: number

  @belongsTo(() => UserMkm, {
    foreignKey: 'user_mkms_id'
  })
  public user: BelongsTo<typeof UserMkm>

  @column.dateTime({ autoCreate: true })
  public createdAt: DateTime

  @column.dateTime({ autoCreate: true, autoUpdate: true })
  public updatedAt: DateTime
}
