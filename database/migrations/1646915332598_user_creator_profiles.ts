import BaseSchema from '@ioc:Adonis/Lucid/Schema'

export default class UserCreatorProfiles extends BaseSchema {
  protected tableName = 'user_creator_profiles'

  public async up () {
    this.schema.createTable(this.tableName, (table) => {
      table.increments('id').primary()

      table.string('poto_profil')
      table.string('kelas')
      table.string('nomor_telepoon')
      table.text('alamat')
      table.string('domisili')
      table.text('deskripsi')

      table.integer('user_creators_id').unsigned().references('id').inTable('user_creators')

      /**
       * Uses timestamptz for PostgreSQL and DATETIME2 for MSSQL
       */
      table.timestamp('created_at', { useTz: true }).nullable()
      table.timestamp('updated_at', { useTz: true }).nullable()
    })
  }

  public async down () {
    this.schema.dropTable(this.tableName)
  }
}
