import BaseSchema from '@ioc:Adonis/Lucid/Schema'

export default class Youtubes extends BaseSchema {
  protected tableName = 'youtubes'

  public async up () {
    this.schema.createTable(this.tableName, (table) => {
      table.increments('id').primary()

      table.string('nama_akun')
      table.string('spesialisasi_bidang')
      table.integer('jumlah_follower')

      table.integer('range_harga_video')
      table.integer('range_harga_short')

      table.integer('user_creator_profiles_id').unsigned().references('id').inTable('user_creator_profiles')
      table.integer('bidang_kontens_id').unsigned().references('id').inTable('bidang_kontens')

      /**
       * Uses timestamptz for PostgreSQL and DATETIME2 for MSSQL
       */
      table.timestamp('created_at', { useTz: true }).nullable()
      table.timestamp('updated_at', { useTz: true }).nullable()
    })
  }

  public async down () {
    this.schema.dropTable(this.tableName)
  }
}
