import BaseSchema from '@ioc:Adonis/Lucid/Schema'

export default class PengajuanKelas extends BaseSchema {
  protected tableName = 'pengajuan_kelas'

  public async up () {
    this.schema.createTable(this.tableName, (table) => {
      table.increments('id').primary()


      table.enum('status', ['none', 'wait', 'accept', 'reject'])

      table.integer('user_creator_profiles_id').unsigned().references('id').inTable('user_creator_profiles')
      /**
       * Uses timestamptz for PostgreSQL and DATETIME2 for MSSQL
       */
      table.timestamp('created_at', { useTz: true }).nullable()
      table.timestamp('updated_at', { useTz: true }).nullable()
    })
  }

  public async down () {
    this.schema.dropTable(this.tableName)
  }
}
