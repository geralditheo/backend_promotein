import BaseSchema from '@ioc:Adonis/Lucid/Schema'

export default class UserMkms extends BaseSchema {
  protected tableName = 'user_mkms'

  public async up () {
    this.schema.createTable(this.tableName, (table) => {
      table.increments('id').primary()

      table.string('username', 255)
      table.string('email', 255)
      table.string('password', 100)

      /**
       * Uses timestamptz for PostgreSQL and DATETIME2 for MSSQL
       */
      table.timestamp('created_at', { useTz: true }).nullable()
      table.timestamp('updated_at', { useTz: true }).nullable()
    })
  }

  public async down () {
    this.schema.dropTable(this.tableName)
  }
}
