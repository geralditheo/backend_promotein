import BaseSchema from '@ioc:Adonis/Lucid/Schema'

export default class UserMkmTokens extends BaseSchema {
  protected tableName = 'user_mkm_tokens'

  public async up () {
    this.schema.createTable(this.tableName, (table) => {
      table.increments('id').primary()

      table.string('name')
      table.string('type')
      table.string('token')

      table.integer('user_mkms_id').unsigned().references('id').inTable('user_mkms')

      /**
       * Uses timestamptz for PostgreSQL and DATETIME2 for MSSQL
       */
      table.timestamp('created_at', { useTz: true }).nullable()
      table.timestamp('updated_at', { useTz: true }).nullable()
    })
  }

  public async down () {
    this.schema.dropTable(this.tableName)
  }
}
