import BaseSchema from '@ioc:Adonis/Lucid/Schema'

export default class UserMkmProfiles extends BaseSchema {
  protected tableName = 'user_mkm_profiles'

  public async up () {
    this.schema.createTable(this.tableName, (table) => {
      table.increments('id').primary()

      table.string('poto_profile')
      table.string('jenis_usaha')
      table.string('target_marketing')
      table.string('no_telepon')
      table.text('alamat')

      table.integer('user_mkms_id').unsigned().references('id').inTable('user_mkms')

      /**
       * Uses timestamptz for PostgreSQL and DATETIME2 for MSSQL
       */
      table.timestamp('created_at', { useTz: true }).nullable()
      table.timestamp('updated_at', { useTz: true }).nullable()
    })
  }

  public async down () {
    this.schema.dropTable(this.tableName)
  }
}
