import BaseSchema from '@ioc:Adonis/Lucid/Schema'

export default class Orders extends BaseSchema {
  protected tableName = 'orders'

  public async up () {
    this.schema.createTable(this.tableName, (table) => {
      table.increments('id').primary()

      table.string('kunjungan_ke_lokasi')
      table.text('ide_promosi')
      table.text('catatan')
      table.date('deadline')

      table.integer('user_mkms_id').unsigned().references('id').inTable('user_mkms')
      table.integer('user_creators_id').unsigned().references('id').inTable('user_creators')

      /**
       * Uses timestamptz for PostgreSQL and DATETIME2 for MSSQL
       */
      table.timestamp('created_at', { useTz: true }).nullable()
      table.timestamp('updated_at', { useTz: true }).nullable()
    })
  }

  public async down () {
    this.schema.dropTable(this.tableName)
  }
}
