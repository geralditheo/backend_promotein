import BaseSchema from '@ioc:Adonis/Lucid/Schema'

export default class PengirimanBarangs extends BaseSchema {
  protected tableName = 'pengiriman_barangs'

  public async up () {
    this.schema.createTable(this.tableName, (table) => {
      table.increments('id').primary()

      table.string('nomor_resi')
      table.string('jenis_pengiriman')
      table.string('keadaan_barang')

      table.integer('order_user_mkms_id').unsigned().references('	user_mkms_id').inTable('orders')
      table.integer('order_user_creators_id').unsigned().references('user_creators_id').inTable('orders')
      table.integer('order_id').unsigned().references('id').inTable('orders')

      /**
       * Uses timestamptz for PostgreSQL and DATETIME2 for MSSQL
       */
      table.timestamp('created_at', { useTz: true }).nullable()
      table.timestamp('updated_at', { useTz: true }).nullable()
    })
  }

  public async down () {
    this.schema.dropTable(this.tableName)
  }
}
