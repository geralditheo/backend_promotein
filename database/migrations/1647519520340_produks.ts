import BaseSchema from '@ioc:Adonis/Lucid/Schema'

export default class Produks extends BaseSchema {
  protected tableName = 'produks'

  public async up () {
    this.schema.createTable(this.tableName, (table) => {
      table.increments('id').primary()

      table.string('poto_produk')
      table.string('jenis_produk')
      table.text('deskripsi_produk')

      table.integer('order_id').unsigned().references('id').inTable('orders')


      /**
       * Uses timestamptz for PostgreSQL and DATETIME2 for MSSQL
       */
      table.timestamp('created_at', { useTz: true }).nullable()
      table.timestamp('updated_at', { useTz: true }).nullable()
    })
  }

  public async down () {
    this.schema.dropTable(this.tableName)
  }
}
