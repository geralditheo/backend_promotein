import BaseSchema from '@ioc:Adonis/Lucid/Schema'

export default class PesanObjeks extends BaseSchema {
  protected tableName = 'pesan_objeks'

  public async up () {
    this.schema.createTable(this.tableName, (table) => {
      table.increments('id').primary()

      table.enum('tiktok', ['true', 'false', 'none'])
      table.enum('youtube_video', ['true', 'false', 'none'])
      table.enum('youtube_short', ['true', 'false', 'none'])
      table.enum('insta_story', ['true', 'false', 'none'])
      table.enum('insta_reels', ['true', 'false', 'none'])
      table.enum('insta_feed', ['true', 'false', 'none'])
      table.enum('all', ['true', 'false', 'enum'])

      table.integer('order_id').unsigned().references('id').inTable('orders')

      /**
       * Uses timestamptz for PostgreSQL and DATETIME2 for MSSQL
       */
      table.timestamp('created_at', { useTz: true }).nullable()
      table.timestamp('updated_at', { useTz: true }).nullable()
    })
  }

  public async down () {
    this.schema.dropTable(this.tableName)
  }
}
