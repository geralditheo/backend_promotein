import BaseSchema from '@ioc:Adonis/Lucid/Schema'

export default class PengajuanSaldos extends BaseSchema {
  protected tableName = 'pengajuan_saldos'

  public async up () {
    this.schema.createTable(this.tableName, (table) => {
      table.increments('id').primary()

      table.enum('melalui', ['bank', 'gopay', 'ovo', 'pulsa'])
      table.string('nomor_rekening')
      table.enum('status', ['accepted', 'rejected', 'none'])

      table.integer('user_creator_profiles_id').unsigned().references('id').inTable('user_creator_profiles')

      /**
       * Uses timestamptz for PostgreSQL and DATETIME2 for MSSQL
       */
      table.timestamp('created_at', { useTz: true }).nullable()
      table.timestamp('updated_at', { useTz: true }).nullable()
    })
  }

  public async down () {
    this.schema.dropTable(this.tableName)
  }
}
