import BaseSeeder from '@ioc:Adonis/Lucid/Seeder'
import BidangKonten from 'App/Models/BidangKonten'


export default class BidangKontenSeeder extends BaseSeeder {
  public async run () {
    // Write your database queries inside the run method

    await BidangKonten.createMany([
      {
        nama: '-none-'
      },
      {
        nama: 'Vlogger'
      },
      {
        nama: 'Beauty Vlogger'
      },
      {
        nama: 'Edukator'
      },
      {
        nama: 'Reviewer'
      },
      {
        nama: 'Gamer'
      },
      {
        nama: 'Komedian'
      },
      {
        nama: 'StoryTeller'
      },
      {
        nama: 'Seniman'
      },
      {
        nama: 'Musisi'
      },
      {
        nama: 'Public Speaker'
      },
      {
        nama: 'Writer'
      },
      {
        nama: 'Visualisator'
      },
      {
        nama: 'Food Vlogger'
      },
      {
        nama: 'Food Blogger'
      },
      {
        nama: 'Padcaster'
      },
      {
        nama: 'Videographer'
      },
    ])
  }
}
