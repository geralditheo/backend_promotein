/*
|--------------------------------------------------------------------------
| Routes
|--------------------------------------------------------------------------
|
| This file is dedicated for defining HTTP routes. A single file is enough
| for majority of projects, however you can define routes in different
| files and just make sure to import them inside this file. For example
|
| Define routes in following two files
| ├── start/routes/cart.ts
| ├── start/routes/customer.ts
|
| and then import them inside `start/routes.ts` as follows
|
| import './routes/cart'
| import './routes/customer'
|
*/

import Route from '@ioc:Adonis/Core/Route'
import HealthCheck from '@ioc:Adonis/Core/HealthCheck'
import UserCreatorsController from 'App/Controllers/Http/UserCreatorsController'
import UserCreatorProfilesController from 'App/Controllers/Http/UserCreatorProfilesController'
import BidangKontensController from 'App/Controllers/Http/BidangKontensController'
import InstagramsController from 'App/Controllers/Http/InstagramsController'
import YoutubesController from 'App/Controllers/Http/YoutubesController'
import TiktoksController from 'App/Controllers/Http/TiktoksController'
import PengjuanKelasController from 'App/Controllers/Http/PengjuanKelasController'
import UserMkmsController from 'App/Controllers/Http/UserMkmsController'
import UserMkmProfilesController from 'App/Controllers/Http/UserMkmProfilesController'
import OrdersController from 'App/Controllers/Http/OrdersController'
import PengirimanBarangsController from 'App/Controllers/Http/PengirimanBarangsController'
import StatusesController from 'App/Controllers/Http/StatusesController'
import ProduksController from 'App/Controllers/Http/ProduksController'
import TransaksisController from 'App/Controllers/Http/TransaksisController'
import PesanObjeksController from 'App/Controllers/Http/PesanObjeksController'
import PengajuanSaldosController from 'App/Controllers/Http/PengajuanSaldosController'
import OrderIndicatorsController from 'App/Controllers/Http/OrderIndicatorsController'
import UserCreatorGetsController from 'App/Controllers/Http/UserCreatorGetsController'
import CouponsController from 'App/Controllers/Http/CouponsController'


Route.get('/', async () => {
  return { message: 'Welcome to Promotein' }
})

Route.group(() => {
  Route.group(() => {
    Route.group(() => {

      Route.post('register', 'UserCreatorsController.register').as('register')
      Route.post('login', 'UserCreatorsController.login').as('login')
      Route.post('logout', 'UserCreatorsController.logout' ).as('logout').middleware('creator')
      
    }).prefix('creator').as('creator')


    Route.group(() => {
      Route.post('register', 'UserMkmsController.register' ).as('register')
      Route.post('login', 'UserMkmsController.login' ).as('login')
      Route.post('logout', 'UserMkmsController.logout' ).as('logout').middleware('mkm')
    }).prefix('mkm').as('mkm')

  }).prefix('auth').as('auth')

  Route.group(() => {
    Route.group(() => {

      Route.group(() => {

        Route.get('', 'UserCreatorProfilesController.index' ).as('index')
        Route.post('', 'UserCreatorProfilesController.store' ).as('store').middleware('creator')
        Route.get(':user_creators_id', 'UserCreatorProfilesController.show' ).as('show').middleware('creator')
        Route.put(':user_creators_id', 'UserCreatorProfilesController.update' ).as('update')
        Route.delete(':user_creators_id', 'UserCreatorProfilesController.destroy' ).as('destroy')

      }).prefix('profile').as('profile')

      Route.group(() => {
        Route.get('', 'InstagramsController.index' ).as('index')
        Route.post('', 'InstagramsController.store').as('store')
        Route.get(':user_creators_id', 'InstagramsController.show').as('show')
        Route.put(':user_creators_id', 'InstagramsController.update').as('update')
        Route.delete(':user_creators_id', 'InstagramsController.destroy').as('destroy')
      }).prefix('instagram').as('instagram')

      Route.group(() => {
        Route.get('', 'YoutubesController.index').as('index')
        Route.post('', 'YoutubesController.store').as('store')
        Route.get(':user_creators_id', 'YoutubesController.show').as('show')
        Route.put(':user_creators_id', 'YoutubesController.update').as('update')
        Route.delete(':user_creators_id', 'YoutubesController.destroy').as('destroy')
      }).prefix('youtube').as('youtube')

      Route.group(() => {
        Route.get('', 'TiktoksController.index' ).as('index')
        Route.post('', 'TiktoksController.store' ).as('store')
        Route.get(':user_creators_id', 'TiktoksController.show' ).as('show')
        Route.put(':user_creators_id', 'TiktoksController.update' ).as('update')
        Route.delete(':user_creators_id', 'TiktoksController.destroy' ).as('destroy')
      }).prefix('tiktok').as('tiktok')

      Route.group(() => {
        Route.get('', 'PengjuanKelasController.index' ).as('index')
        Route.post('', 'PengjuanKelasController.store' ).as('store')
        Route.get(':user_creators_id', 'PengjuanKelasController.show' ).as('show')
        Route.put(':user_creators_id', 'PengjuanKelasController.update' ).as('update')
        Route.delete(':user_creators_id', 'PengjuanKelasController.destroy' ).as('destroy')
      }).prefix('pengajuan').as('pengajuan')

      Route.group(() => {

        Route.get('', 'PengajuanSaldosController.index').as('index')
        Route.post('', 'PengajuanSaldosController.store').as('store')
        Route.get(':user_creators_id', 'PengajuanSaldosController.show').as('show')
        Route.put(':user_creators_id', 'PengajuanSaldosController.update').as('update')
        Route.delete(':user_creators_id', 'PengajuanSaldosController.destroy').as('destroy')

      }).prefix('pengajuan_saldo').as('pengajuan_saldo')

      Route.group(() => {

        Route.get('', 'UserCreatorGetsController.index' ).as('index')
        Route.get('limit', 'UserCreatorGetsController.limit' ).as('limit')
        Route.get('profile/:id', 'UserCreatorGetsController.profile' ).as('profile')
        Route.get('instagram/:id', 'UserCreatorGetsController.instagram' ).as('instagram')
        Route.get('youtube/:id', 'UserCreatorGetsController.youtube').as('youtube')
        Route.get(':id', 'UserCreatorGetsController.show' ).as('show')
        Route.get('priceAll/:id', 'UserCreatorGetsController.priceAll' ).as('priceAll')
        
        Route.post('image', 'UserCreatorGetsController.image' ).as('image')

      }).prefix('get').as('get')


    }).prefix('creator').as('creator')


    Route.group(() => {
      Route.group(() => {
        Route.get('', 'UserMkmProfilesController.index').as('index')
        Route.post('', 'UserMkmProfilesController.store').as('store').middleware('mkm')
        Route.get(':user_mkms_id', 'UserMkmProfilesController.show').as('show').middleware('mkm')
        Route.put(':user_mkms_id', 'UserMkmProfilesController.update').as('update').middleware('mkm')
        Route.delete(':user_mkms_id', 'UserMkmProfilesController.destroy').as('destroy')
      }).prefix('profile').as('profile')
    }).prefix('mkm').as('mkm')

  }).prefix('user').as('user')

  Route.resource('bidang', 'BidangKontensController').apiOnly().as('bidang')

  Route.group(() => {

    Route.group(() => {

      Route.get(':user_creators_id', 'OrdersController.creator' ).as('creator')
      Route.get('accept/:user_creators_id', 'OrdersController.order_creator_accepted' ).as('creator.accept')

    }).prefix('creator')

    Route.group(() => {

      Route.get(':user_mkms_id', 'OrdersController.mkm' ).as('mkm')

    }).prefix('mkm')

    Route.group(() => {

      Route.get('', 'PengirimanBarangsController.index' ).as('index')
      Route.post('', 'PengirimanBarangsController.store' ).as('store')
      Route.get(':order_id', 'PengirimanBarangsController.show' ).as('show')
      Route.put(':order_id', 'PengirimanBarangsController.update' ).as('update')
      Route.delete(':order_id', 'PengirimanBarangsController.destroy' ).as('destroy')
    
    }).prefix('pengiriman_barang').as('pengiriman_barang')

    Route.group(() => {

      Route.get('', 'StatusesController.index' ).as('index')
      Route.post('', 'StatusesController.store' ).as('store')
      Route.get(':status_id', 'StatusesController.show' ).as('show')
      Route.put(':status_id', 'StatusesController.update' ).as('update')
      Route.delete(':status_id', 'StatusesController.destroy' ).as('destroy')
      
    }).prefix('status').as('status');

    Route.group(() => {
      
      Route.get('', 'ProduksController.index' ).as('index')
      Route.post('', 'ProduksController.store' ).as('store')
      Route.get(':produk_id', 'ProduksController.show' ).as('show')
      Route.put(':produk_id', 'ProduksController.update' ).as('update')
      Route.delete(':produk_id', 'ProduksController.destroy' ).as('destroy')

      Route.post('image/:produk_id', 'ProduksController.image' ).as('image')

    }).prefix('produk').as('produk')

    Route.group(() => {

      Route.get('', 'TransaksisController.index' ).as('index')
      Route.post('', 'TransaksisController.store' ).as('store')
      Route.get(':transaksi_id', 'TransaksisController.show' ).as('show')
      Route.put(':transaksi_id', 'TransaksisController.update' ).as('update')
      Route.delete(':transaksi_id', 'TransaksisController.destroy' ).as('destroy')

    }).prefix('transaksi').as('transaksi')

    Route.group(() => {

      Route.get('', 'PesanObjeksController.index').as('index')
      Route.post('', 'PesanObjeksController.store').as('store')
      Route.get(':objek_id', 'PesanObjeksController.show').as('show')
      Route.put(':objek_id', 'PesanObjeksController.update').as('update')
      Route.delete(':objek_id', 'PesanObjeksController.destroy').as('detroy')

    }).prefix('pesan_objek').as('pesan_objek')

    Route.get('', 'OrdersController.index').as('index')
    Route.post('', 'OrdersController.store').as('store')
    Route.get(':order_id', 'OrdersController.show').as('show').where('order_id', /^[0-9]+$/ )
    Route.put(':order_id', 'OrdersController.update').as('update')
    Route.delete(':order_id', 'OrdersController.destroy').as('destroy')

  }).prefix('order').as('order')

  Route.group(() => {

    Route.get('', 'CouponsController.index' ).as('index')
    Route.post('', 'CouponsController.store' ).as('store')
    Route.get(':coupon_id', 'CouponsController.show' ).as('show')
    Route.put(':coupon_id', 'CouponsController.update' ).as('update')
    Route.delete(':coupon_id', 'CouponsController.destroy' ).as('destroy')
    Route.post('check', 'CouponsController.check' ).as('check')
    Route.post('useCoupon', 'CouponsController.useCoupon' ).as('use')

  }).prefix('coupon').as('coupon')

  Route.group(() => {

    Route.group(() => {

      Route.post('', 'OrderIndicatorsController.store' ).as('store')
      Route.put(':indicator_id', 'OrderIndicatorsController.update' ).as('update')
      
    }).prefix('indicator').as('indicator')

  }).prefix('admin').as('admin')

}).prefix('api').as('api')

Route.get('health', async ({ response }) => {
  const report = await HealthCheck.getReport()

  return report.healthy
    ? response.ok(report)
    : response.badRequest(report)

}).as('health')